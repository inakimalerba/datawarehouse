"""KCIDB Views."""
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseForbidden
from django.http import HttpResponseRedirect
from django.template import loader

from . import models
from . import pagination


def revisions_list(request):
    """Get list of revisions."""
    template = loader.get_template('web/kcidb/revisions.html')
    page = request.GET.get('page')

    paginator = pagination.EndlessPaginator(
        models.KCIDBRevision.objects.values_list('iid', flat=True),
        30
    )
    revision_iids = paginator.get_page(page)

    revisions = (
        models.KCIDBRevision.objects
        .aggregated()
        .filter(iid__in=revision_iids)
        .prefetch_related(
            'tree',
            'mergerun_set',
            'mergerun_set__pipeline',
        )
    )

    context = {
        'revisions': revisions,
        'paginator': revision_iids,
    }

    return HttpResponse(template.render(context, request))


def revisions_list_by_failure(request, stage):
    """Show failed revisions classified by stage."""
    template = loader.get_template('web/kcidb/revisions_failures.html')
    page = request.GET.get('page')

    filters = {
        'revision': {
            'filter': {'valid': False},
        },
        'build': {
            'filter': {'kcidbbuild__valid': False},
        },
        'test': {
            'filter': {'kcidbbuild__kcidbtest__status__name__in': models.KCIDBTest.UNSUCCESSFUL_STATUSES},
            'exclude': {'kcidbbuild__kcidbtest__waived': True},
        }
    }

    if stage not in ['all'] + list(filters):
        return HttpResponseBadRequest(f'Not sure what {stage} is.')

    stages = list(filters) if stage == 'all' else [stage]

    # Get a list of iids of the revisions with failures
    revision_iids = []
    for stage_name in stages:
        revision_iids.extend(
            list(
                models.KCIDBRevision.objects
                .filter(**filters[stage_name].get('filter', {}))
                .exclude(**filters[stage_name].get('exclude', {}))
                .values_list('iid', flat=True)
            )
        )

    # Sort ids descendingly and remove duplicates
    revision_iids = sorted(list(set(revision_iids)), reverse=True)

    paginator = pagination.EndlessPaginator(revision_iids, 30)
    revision_iids_page = paginator.get_page(page)

    revisions = (
        models.KCIDBRevision.objects
        .aggregated()
        .filter(iid__in=revision_iids_page)
        .prefetch_related(
            'tree',
            'mergerun_set',
            'mergerun_set__pipeline',
        )
    )

    context = {
        'paginator': revision_iids_page,
        'revisions': revisions,
        'stage_filter': stage,
        'stages': list(filters),
    }

    return HttpResponse(template.render(context, request))


def revisions_get(request, revision_iid):
    """Get a single revision."""
    template = loader.get_template('web/kcidb/revision.html')

    revision = (
        models.KCIDBRevision.objects.aggregated()
        .select_related(
            'log',
            'origin',
            'tree',
        )
        .prefetch_related(
            'patches',
            'kcidbbuild_set',
            'kcidbbuild_set__architecture',
            'kcidbbuild_set__kcidbtest_set',
            'kcidbbuild_set__kcidbtest_set__status',
            'kcidbbuild_set__kcidbtest_set__test',
        )
        .get(iid=revision_iid)
    )

    tests = (
        models.KCIDBTest.objects
        .filter(
            build__revision=revision
        )
        .select_related(
            'build',
            'status',
            'test',
        )
        .prefetch_related(
            'issues',
            'build__architecture',
        )
    )

    builds = (
        models.KCIDBBuild.objects
        .filter(
            revision=revision
        )
        .select_related(
            'architecture',
        )
        .prefetch_related(
            'kcidbtest_set',
            'kcidbtest_set__status',
            'kcidbtest_set__test',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved=False
        )
        .select_related(
            'kind'
        )
    )

    issue_occurrences = [
        {
            'issue': issue,
            'revisions': (
                issue.kcidbrevision_set.filter(iid=revision.iid)
            ),
            'builds': (
                issue.kcidbbuild_set
                .filter(revision=revision)
                .select_related(
                    'architecture',
                )
            ),
            'tests': (
                issue.kcidbtest_set
                .filter(build__revision=revision)
                .select_related(
                    'status',
                    'test',
                )
            ),
        } for issue in models.Issue.objects.filter(
            Q(kcidbrevision=revision) |
            Q(kcidbbuild__revision=revision) |
            Q(kcidbtest__build__revision=revision)
        ).select_related(
            'kind'
        ).distinct().order_by('-id')  # Ordering not respected by distinct.
    ]

    context = {
        'builds': builds,
        'builds_failed': builds.exclude(valid=True),
        'issues': issues,
        'issue_occurrences': issue_occurrences,
        'revision': revision,
        'revisions_failed': [revision] if not revision.valid else [],
        'tests': tests,
        'tests_failed': tests.exclude(status__name='PASS'),
    }

    return HttpResponse(template.render(context, request))


def builds_get(request, build_iid):
    """Get a single build."""
    template = loader.get_template('web/kcidb/build.html')

    build = (
        models.KCIDBBuild.objects.aggregated()
        .select_related(
            'architecture',
            'compiler',
            'log',
            'origin',
            'revision',
        )
        .prefetch_related(
            'input_files',
            'output_files',
            'kcidbtest_set',
            'kcidbtest_set__output_files',
            'kcidbtest_set__status',
            'kcidbtest_set__test',
        )
        .get(iid=build_iid)
    )

    tests = (
        models.KCIDBTest.objects
        .filter(
            build=build
        )
        .select_related(
            'build',
            'status',
            'test',
        )
        .prefetch_related(
            'issues',
            'build__architecture',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved=False
        )
        .select_related(
            'kind'
        )
    )

    issue_occurrences = [
        {
            'issue': issue,
            'builds': (
                issue.kcidbbuild_set
                .filter(iid=build.iid)
            ),
            'tests': (
                issue.kcidbtest_set
                .filter(build=build)
                .select_related(
                    'status',
                    'test',
                )
            ),
        } for issue in models.Issue.objects.filter(
            Q(kcidbbuild=build) |
            Q(kcidbtest__build=build)
        ).select_related(
            'kind'
        ).distinct().order_by('-id')  # Ordering not respected by distinct.
    ]

    context = {
        'build': build,
        'builds_failed': [build] if not build.valid else [],
        'issues': issues,
        'issue_occurrences': issue_occurrences,
        'tests': tests,
        'tests_failed': tests.exclude(status__name='PASS'),
    }

    return HttpResponse(template.render(context, request))


def tests_get(request, test_iid):
    """Get a single test."""
    template = loader.get_template('web/kcidb/test.html')

    test = (
        models.KCIDBTest.objects
        .select_related(
            'build',
            'build__revision',
            'environment',
            'origin',
            'status',
            'test',
        )
        .prefetch_related(
            'output_files',
        )
        .get(iid=test_iid)
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved=False
        )
        .select_related(
            'kind'
        )
    )

    issue_occurrences = [
        {
            'tests': (
                issue.kcidbtest_set
                .filter(iid=test.iid)
            ),
            'issue': issue
        } for issue in models.Issue.objects.filter(
            kcidbtest=test
        ).select_related(
            'kind'
        ).distinct().order_by('-id')  # Ordering not respected by distinct.
    ]

    context = {
        'test': test,
        'tests_failed': [test] if not test.status or test.status.name != 'PASS' else [],
        'issues': issues,
        'issue_occurrences': issue_occurrences,
    }

    return HttpResponse(template.render(context, request))


def kcidb_issue(request):
    """Link/Unlink revisions, builds or tests to a given issue."""
    if request.method == "POST":
        objects = {
            'revision': {
                'permission': 'datawarehouse.change_kcidbrevision',
                'elements': models.KCIDBRevision.objects.filter(
                    iid__in=request.POST.getlist('revision_iids')
                ),
            },
            'build': {
                'permission': 'datawarehouse.change_kcidbbuild',
                'elements': models.KCIDBBuild.objects.filter(
                    iid__in=request.POST.getlist('build_iids')
                ),
            },
            'test': {
                'permission': 'datawarehouse.change_kcidbtest',
                'elements': models.KCIDBTest.objects.filter(
                    iid__in=request.POST.getlist('test_iids')
                ),
            },
        }

        # Check all permissions before performing any change.
        for obj in objects.values():
            if obj['elements'] and not request.user.has_perm(obj['permission']):
                return HttpResponseForbidden()

        issue_id = request.POST.get('issue_id')
        issue = models.Issue.objects.get(id=issue_id)

        action = request.POST.get('action', 'add')
        for obj in objects.values():
            for element in obj['elements']:
                if action == 'add':
                    element.issues.add(issue)
                elif action == 'remove':
                    element.issues.remove(issue)
                else:
                    HttpResponseBadRequest(f'Action {action} unknown.')

    return HttpResponseRedirect(request.POST.get('redirect_to'))

"""Helpers."""
from datawarehouse import models


def get_failed_pipelines(group):
    """Get a list of failed pipelines."""
    failures = (models.Pipeline.objects
                .add_stats()
                .prefetch_related('lint_jobs',
                                  'merge_jobs',
                                  'build_jobs',
                                  'test_jobs',
                                  ))

    if group == 'all':
        failures = failures.exclude(stats_lint_fail_count=0,
                                    stats_merge_fail_count=0,
                                    stats_build_fail_count=0,
                                    stats_test_fail_count=0,
                                    stats_test_error_count=0)
    elif group == 'test':
        failures = failures.exclude(stats_test_fail_count=0,
                                    stats_test_error_count=0)
    else:
        failures = failures.exclude(**{f'stats_{group}_fail_count': 0})

    return failures

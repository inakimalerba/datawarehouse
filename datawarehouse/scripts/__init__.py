# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2019 Red Hat, Inc.
"""Scripts file."""
from cki_lib.timer import ScheduledTask
from django.conf import settings

from .metrics import get_pipelines_metrics
from .metrics import get_series_metrics
from .metrics import get_tests_metrics
from .misc import LOGGER
from .misc import send_kcidb_object_for_retriage

TIMER_RETRIAGE = ScheduledTask(
    settings.TIMER_RETRIAGE_PERIOD_S,
    send_kcidb_object_for_retriage.delay)

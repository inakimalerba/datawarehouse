# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
import datetime

from celery import shared_task
from cki_lib.logger import get_logger
from django.utils import timezone

from datawarehouse import models
from datawarehouse import signals

LOGGER = get_logger(__name__)


@shared_task
def send_kcidb_object_for_retriage(since_days_ago=14):
    """Add last n days objects to the queue for triaging."""
    date_from = timezone.now() - datetime.timedelta(days=since_days_ago)

    to_retriage = {
        'revision': models.KCIDBRevision.objects.filter(
            valid=False,
            discovery_time__gte=date_from,
        ),
        'build': models.KCIDBBuild.objects.filter(
            valid=False,
            start_time__gte=date_from
        ),
        'test': models.KCIDBTest.objects.filter(
            status__name__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES,
            start_time__gte=date_from
        ),
    }

    for kind, objects in to_retriage.items():
        signals.kcidb_object.send(
            sender='scripts.misc.send_kcidb_object_for_retriage',
            status='needs_triage',
            object_type=kind,
            objects=objects,
        )

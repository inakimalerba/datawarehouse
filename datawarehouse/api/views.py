"""API Views."""
import datetime
import email.utils
import mailbox

from cki_lib.misc import strtobool
from django.db.models import Q
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from datawarehouse import models
from datawarehouse import pagination
from datawarehouse import serializers
from datawarehouse import utils


class Issue(APIView):
    """Endpoint for Issue handling."""

    queryset = models.Issue.objects.none()

    def get(self, request, issue_id=None):
        """GET request."""
        if issue_id:
            return self._get(request, issue_id)

        return self._list(request)

    def _get(self, request, issue_id):
        """Get a single Issue."""
        try:
            issue = models.Issue.objects.get(id=issue_id)
        except models.Issue.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_issue = serializers.IssueSerializer(issue).data
        return Response(serialized_issue)

    def _list(self, request):
        """Get a list of Issues. Optionally filter by resolved status."""
        resolved = request.GET.get('resolved')

        issues = models.Issue.objects.all()

        if resolved:
            issues = issues.filter(resolved=strtobool(resolved))

        paginator = pagination.DatawarehousePagination()
        paginated_issues = paginator.paginate_queryset(issues, request)
        serialized_issues = serializers.IssueSerializer(paginated_issues, many=True).data

        context = {
            'issues': serialized_issues,
        }

        return paginator.get_paginated_response(context)


class Pipeline(APIView):
    """Get pipeline data."""

    queryset = models.Pipeline.objects.none()

    def get(self, request, pipeline_id):
        """Get information of a pipeline."""
        style = request.GET.get('style')

        try:
            pipe = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if style == 'reporter':
            serializer = serializers.PipelineOrderedSerializer
        else:
            serializer = serializers.PipelineSerializer

        return Response(serializer(pipe, context={'request': request}).data)


class PipelineReport(APIView):
    """Reports and pipelines."""

    queryset = models.Report.objects.none()

    def get(self, request, pipeline_id):
        """Get the reports of a pipeline."""
        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        paginator = pagination.DatawarehousePagination()
        paginated_reports = paginator.paginate_queryset(
            pipeline.reports.all(),
            request)

        context = {
            'reports': serializers.ReportSerializer(paginated_reports, many=True).data,
        }

        return paginator.get_paginated_response(context)

    def post(self, request, pipeline_id):
        """Submit a new report for the pipeline."""
        content = request.data.get('content')
        mbox = mailbox.mboxMessage(content)

        addr_to = mbox.get_all('to', [])
        addr_cc = mbox.get_all('cc', [])
        subject = mbox.get('Subject')
        date = mbox.get('Date')
        msgid = mbox.get('Message-ID')

        if models.Report.objects.filter(msgid=msgid).exists():
            return Response("MsgID already exists.", status=status.HTTP_400_BAD_REQUEST)

        if mbox.is_multipart():
            for part in mbox.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))

                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    body = part.get_payload(decode=True).decode()
                    break
        else:
            body = mbox.get_payload(decode=True).decode()

        pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)

        sent_at = email.utils.parsedate_to_datetime(date)
        try:
            sent_at = timezone.make_aware(sent_at)
        except ValueError:
            # tzinfo is already set
            pass

        report = models.Report.objects.create(
            pipeline=pipeline,
            subject=subject,
            body=body,
            msgid=msgid,
            sent_at=sent_at,
            raw=content,
        )

        for _, addr in email.utils.getaddresses(addr_to):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_to.add(recipient)

        for _, addr in email.utils.getaddresses(addr_cc):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_cc.add(recipient)

        serialized_report = serializers.ReportSerializer(report).data

        return Response(serialized_report, status=status.HTTP_201_CREATED)


class ReportMissing(APIView):
    """Get pipeline without report sent."""

    queryset = models.Report.objects.none()

    def get(self, request):
        """Get list of pipelines without reports."""
        since = request.GET.get('since')

        # Exclude newer than 3 hours as grace time.
        # gitlab.cee.redhat.com/cki-project/deployment/datawarehouse-report-crawler
        last_3_hours = timezone.now() - datetime.timedelta(hours=3)

        pipelines = (models.Pipeline.objects.add_stats()
                     .filter(stats_report_exists=False)
                     .exclude(Q(duration=None) | Q(finished_at__gte=last_3_hours))
                     )

        if since:
            since = utils.timestamp_to_datetime(since)
            pipelines = pipelines.filter(created_at__gte=since)

        paginator = pagination.DatawarehousePagination()
        paginated_pipelines = paginator.paginate_queryset(pipelines, request)
        serialized_pipelines = serializers.PipelineSimpleSerializer(paginated_pipelines, many=True).data

        context = {
            'pipelines': serialized_pipelines,
        }

        return paginator.get_paginated_response(context)


class TestSigle(APIView):
    """Endpoint for handling single test."""

    queryset = models.Test.objects.none()

    def get(self, request, test_id):
        """Return a single test."""
        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_test = serializers.TestSerializer(test).data

        return Response(serialized_test)


class TestList(APIView):
    """Endpoint for handling many tests."""

    queryset = models.Test.objects.none()

    def get(self, request):
        """Return a list of all the tests."""
        tests = models.Test.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_tests = paginator.paginate_queryset(tests, request)

        serialized_tests = serializers.TestSerializer(paginated_tests, many=True).data

        context = {
            'tests': serialized_tests,
        }

        return paginator.get_paginated_response(context)


class IssueRegex(APIView):
    """IssueRegex class."""

    queryset = models.IssueRegex.objects.none()

    def get(self, request):
        """Get IssueRegexes."""
        issue_regexes = models.IssueRegex.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_issue_regexes = paginator.paginate_queryset(issue_regexes, request)

        serialized_issue_regexes = serializers.IssueRegexSerializer(paginated_issue_regexes, many=True).data

        context = {
            'issue_regexes': serialized_issue_regexes
        }

        return paginator.get_paginated_response(context)


class PipelineJobs(APIView):
    """PipelineJobs class."""

    queryset = models.Pipeline.objects.none()

    stages = {
        'lint': {'model': models.LintRun, 'serializer': serializers.LintRunSerializer},
        'merge': {'model': models.MergeRun, 'serializer': serializers.MergeRunSerializer},
        'build': {'model': models.BuildRun, 'serializer': serializers.BuildRunSerializer},
        'test': {'model': models.TestRun, 'serializer': serializers.TestRunSerializer},
    }

    def get(self, request, pipeline_id, stage_name, success=None):
        """Get jobs from selected stage."""
        stage = self.stages.get(stage_name)
        if not stage:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        jobs = stage['model'].objects.filter(pipeline=pipeline)

        if success is not None:
            if stage_name == 'test':
                jobs = (
                    jobs.filter(passed=success, waived=False, skipped=False)
                    .exclude(result__name='NEW')
                    .select_related('test', 'beakertestrun', 'kernel_arch', 'pipeline__project')
                    .prefetch_related('test__maintainers', 'logs')
                )
            else:
                jobs = jobs.filter(success=success)

        paginator = pagination.DatawarehousePagination()
        paginated_jobs = paginator.paginate_queryset(jobs, request)
        serialized_jobs = stage['serializer'](paginated_jobs, many=True).data

        context = {
            'jobs': serialized_jobs
        }

        return paginator.get_paginated_response(context)


class PipelineFailedJobs(PipelineJobs):
    """Pipeline Failed Jobs."""

    def get(self, request, pipeline_id, stage_name):
        """Get failed jobs for a pipeline."""
        return super(PipelineFailedJobs, self).get(request, pipeline_id, stage_name, success=False)


class PipelinePatches(APIView):
    """PipelinePatches class."""

    queryset = models.Patch.objects.none()

    def get(self, request, pipeline_id):
        """Get patches from pipeline."""
        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        patches = pipeline.patches.all()

        paginator = pagination.DatawarehousePagination()
        paginated_patches = paginator.paginate_queryset(patches, request)
        serialized_patches = serializers.PatchSerializer(paginated_patches, many=True).data

        context = {
            'patches': serialized_patches
        }

        return paginator.get_paginated_response(context)

# pylint: disable=too-few-public-methods
"""Views."""
from collections import defaultdict

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.misc import strtobool
from django.conf import settings
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from datawarehouse import models
from datawarehouse import pagination
from datawarehouse import serializers as dw_serializers
from datawarehouse import utils

from . import serializers

LOGGER = get_logger(__name__)


class Submit(APIView):
    """Submit KCIDB data."""

    queryset = models.KCIDBRevision.objects.none()

    def post(self, request):
        """Post json data."""
        response = defaultdict(list)

        data_models = (
            ('revisions', models.KCIDBRevision),
            ('builds', models.KCIDBBuild),
            ('tests', models.KCIDBTest),
        )

        input_data = request.data.get('data')

        if input_data.get('version') != {'major': 3, 'minor': 0}:
            LOGGER.info('Submitted unknown schema version: %s', input_data.get('version'))
            return HttpResponseBadRequest('Unknown schema version.')

        for key, model in data_models:
            for data in input_data.get(key, []):

                # Do not submit retrigger pipelines unless ALLOW_DEBUG_PIPELINES=True
                is_retrigger = strtobool(get_nested_key(data, 'misc/pipeline/variables/retrigger', 'false'))
                if is_retrigger and not settings.ALLOW_DEBUG_PIPELINES:
                    response['warnings'].append([key, data['id'], 'Retrigger pipeline ignored'])
                    continue

                try:
                    model.create_from_json(data)
                except models.MissingParent as error:
                    response['errors'].append([key, data['id'], str(error)])
                except models.AlreadySubmitted as error:
                    response['warnings'].append([key, data['id'], str(error)])

        if response.get('errors'):
            LOGGER.info('Errors on submission: %s', response['errors'])
            response_status = status.HTTP_400_BAD_REQUEST
        else:
            response_status = status.HTTP_201_CREATED

        return Response(response, status=response_status)


class RevisionGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Revision."""

    serializer_class = serializers.KCIDBRevisionSerializer
    queryset = models.KCIDBRevision.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class RevisionList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Revisions."""

    serializer_class = serializers.KCIDBRevisionSerializer
    queryset = models.KCIDBRevision.objects.all()


class BuildGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Build."""

    serializer_class = serializers.KCIDBBuildSerializer
    queryset = models.KCIDBBuild.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class BuildList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Builds for a given Revision."""

    serializer_class = serializers.KCIDBBuildSerializer
    queryset = models.KCIDBBuild.objects.all()
    lookup_fields = (
        ('id', 'revision__%%id'),
    )


class TestGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Test."""

    serializer_class = serializers.KCIDBTestSerializer
    queryset = models.KCIDBTest.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class TestList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Tests run on a given Build."""

    serializer_class = serializers.KCIDBTestSerializer
    queryset = models.KCIDBTest.objects.all()
    lookup_fields = (
        ('id', 'build__%%id'),
    )


class IssueOccurrenceView(APIView):
    """Issue occurrences management."""

    def _get_object(self, id):
        if id.isdigit() and int(id) < 2e9:
            return get_object_or_404(self.model.objects, iid=id)
        else:
            return get_object_or_404(self.model.objects, id=id)

    def get(self, request, id, issue_id=None, **kwargs):
        """Get or list depending on issue_id being present."""
        if issue_id:
            return self._get(request, id, issue_id)
        else:
            return self._list(request, id)

    def _list(self, request, id, issue_id=None):
        obj = self._get_object(id)
        obj_issues = obj.issues.all()

        paginator = pagination.DatawarehousePagination()
        paginated = paginator.paginate_queryset(obj_issues, request)

        serialized = dw_serializers.IssueSerializer(paginated, many=True).data
        return paginator.get_paginated_response(serialized)

    def _get(self, request, id, issue_id=None):
        obj = self._get_object(id)
        obj_issue = get_object_or_404(obj.issues, id=issue_id)

        serialized = dw_serializers.IssueSerializer(obj_issue).data
        return Response(serialized)

    def post(self, request, id):
        """Mark revision issues."""
        issue_id = request.data.get('issue_id')

        kcidb_test = self._get_object(id)
        issue = get_object_or_404(models.Issue, id=issue_id)
        kcidb_test.issues.add(issue)

        serialized = dw_serializers.IssueSerializer(issue).data
        return Response(serialized, status=status.HTTP_201_CREATED)

    def delete(self, request, id, issue_id):
        """Delete Issue relationship."""
        kcidb_test = self._get_object(id)
        issue = get_object_or_404(models.Issue, id=issue_id)
        kcidb_test.issues.remove(issue)

        return Response(status=status.HTTP_204_NO_CONTENT)


class RevisionIssueOccurrence(IssueOccurrenceView):
    """Revision Issue occurrences management."""

    queryset = models.KCIDBRevision.objects.none()
    model = models.KCIDBRevision


class BuildIssueOccurrence(IssueOccurrenceView):
    """Build Issue occurrences management."""

    queryset = models.KCIDBBuild.objects.none()
    model = models.KCIDBBuild


class TestIssueOccurrence(IssueOccurrenceView):
    """Test Issue occurrences management."""

    queryset = models.KCIDBTest.objects.none()
    model = models.KCIDBTest

"""Urls file."""
from django.urls import include
from django.urls import path

from . import views

urlpatterns = [
    path('1/pipeline/<int:pipeline_id>/report', views.PipelineReport.as_view()),
    path('1/pipeline/<int:pipeline_id>', views.Pipeline.as_view()),
    path('1/pipeline/<int:pipeline_id>/patches', views.PipelinePatches.as_view()),
    path('1/pipeline/<int:pipeline_id>/jobs/<str:stage_name>', views.PipelineJobs.as_view()),
    path('1/pipeline/<int:pipeline_id>/jobs/<str:stage_name>/failures', views.PipelineFailedJobs.as_view()),
    path('1/issue/<int:issue_id>', views.Issue.as_view()),
    path('1/issue', views.Issue.as_view()),
    path('1/issue/regex', views.IssueRegex.as_view()),
    path('1/report/missing', views.ReportMissing.as_view()),
    path('1/test/<int:test_id>', views.TestSigle.as_view()),
    path('1/test', views.TestList.as_view()),
    path('1/kcidb/', include('datawarehouse.api.kcidb.urls')),
]

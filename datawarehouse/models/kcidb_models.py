"""KCIDB schema models file."""
from email.utils import parseaddr

from cki_lib.misc import get_nested_key
from django.db import models

from datawarehouse import models as dw_models
from datawarehouse import signals


class AlreadySubmitted(Exception):
    """Object is already present in the DB."""


class MissingParent(Exception):
    """Object's parent is missing."""


def is_cki_submission(data):
    """Return True if this data matches a cki pipeline."""
    misc = data.get('misc', {})
    return {'pipeline', 'job'}.issubset(set(misc.keys()))


class Maintainer(models.Model):
    """Model for Maintainer."""

    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_address(cls, address):
        """Create Maintainer from address."""
        name, email = parseaddr(address)
        maintainer = cls.objects.update_or_create(
            email=email,
            defaults={'name': name},
        )[0]
        return maintainer


class KCIDBOrigin(models.Model):
    """Model for KCIDBOrigin."""

    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        """Return __str__ formatted."""
        return self.name

    @classmethod
    def create_from_string(cls, name):
        """Create KCIDBOrigin from string."""
        return cls.objects.get_or_create(name=name)[0]


class KCIDBRevisionManager(models.Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBRevision."""

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        revision = KCIDBRevision.objects.filter(iid=models.OuterRef('iid'))
        revision_untriaged = revision.filter(
            valid=False,
            issues=None
        )

        builds_run = KCIDBBuild.objects.filter(revision__iid=models.OuterRef('iid'))
        builds_pass = builds_run.filter(valid=True)
        builds_fail = builds_run.filter(valid=False)
        builds_untriaged = builds_fail.filter(issues=None)

        tests_run = KCIDBTest.objects.filter(build__revision__iid=models.OuterRef('iid'))
        tests_pass = tests_run.filter(status__name='PASS')
        tests_skip = tests_run.filter(status__name='SKIP')
        tests_fail = tests_run.filter(status__name='FAIL', waived=False)
        tests_untriaged = (
            KCIDBTest.objects
            .filter(
                build__revision__iid=models.OuterRef('iid'),
                status__name__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
                issues=None
            )
        )

        counts = {
            'builds_run': builds_run,
            'builds_pass': builds_pass,
            'builds_fail': builds_fail,
            'tests_run': tests_run,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
        }

        # Look for issues to determine if there are objects triaged.
        revision_issues = dw_models.Issue.objects.filter(
            kcidbrevision__iid=models.OuterRef('iid')
        )
        build_issues = dw_models.Issue.objects.filter(
            kcidbbuild__revision__iid=models.OuterRef('iid')
        )
        test_issues = dw_models.Issue.objects.filter(
            kcidbtest__build__revision__iid=models.OuterRef('iid')
        )

        annotations = {
            # Child objects passed
            'stats_builds_passed': ~models.Exists(builds_fail),
            'stats_tests_passed': ~models.Exists(tests_fail),

            # Objects have issues
            'stats_revision_triaged': models.Exists(revision_issues),
            'stats_builds_triaged': models.Exists(build_issues),
            'stats_tests_triaged': models.Exists(test_issues),

            # Objects have failures without issues
            'stats_revision_untriaged': models.Exists(revision_untriaged),
            'stats_builds_untriaged': models.Exists(builds_untriaged),
            'stats_tests_untriaged': models.Exists(tests_untriaged),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('iid').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)


class KCIDBRevision(models.Model):
    """Model for KCIDBRevision."""

    iid = models.AutoField(primary_key=True)

    id = models.CharField(max_length=300, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    tree = models.ForeignKey('GitTree', on_delete=models.PROTECT, null=True, blank=True)
    git_repository_url = models.URLField(null=True, blank=True)
    git_repository_branch = models.CharField(max_length=200, null=True, blank=True)
    git_commit_hash = models.CharField(max_length=40, null=True, blank=True)
    git_commit_name = models.CharField(max_length=40, null=True, blank=True)
    patches = models.ManyToManyField('Patch', related_name='revisions', blank=True)
    message_id = models.CharField(null=True, blank=True, max_length=200)
    description = models.TextField(null=True, blank=True)
    publishing_time = models.DateTimeField(null=True, blank=True)
    discovery_time = models.DateTimeField(null=True, blank=True)
    contacts = models.ManyToManyField('Maintainer', related_name='revisions', blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    valid = models.BooleanField(null=True)

    issues = models.ManyToManyField('Issue')

    objects = KCIDBRevisionManager()

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    @property
    def is_public(self):
        """
        Return True if this KCIDBRevision is public.

        If there's no MergeRun associated, or any of the MergeRuns
        associated with this KCIDBRevision has a kernel_type
        different to 'upstream', consider it private.
        """
        return (
            self.mergerun_set.exists() and
            not self.mergerun_set.exclude(
                pipeline__variables__key='kernel_type',
                pipeline__variables__value='upstream'
            ).exists()
        )

    @property
    def is_triaged(self):
        """Return True if this revision is triaged."""
        if hasattr(self, 'stats_revision_triaged'):
            return self.stats_revision_triaged  # pylint: disable=no-member

        return self.issues.exists()

    @property
    def is_missing_triage(self):
        """Return True if this revision is missing triage."""
        if hasattr(self, 'stats_revision_untriaged'):
            return self.stats_revision_untriaged  # pylint: disable=no-member

        return not self.valid and not self.issues.exists()

    @property
    def builds_triaged(self):
        """Return list of triaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_triaged') and not self.stats_builds_triaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.exclude(issues=None)

    @property
    def builds_untriaged(self):
        """Return list of untriaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_untriaged') and not self.stats_builds_untriaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.filter_untriaged()

    @property
    def tests_triaged(self):
        """Return list of triaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_triaged') and not self.stats_tests_triaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter(build__revision=self)
            .exclude(issues=None)
        )

    @property
    def tests_untriaged(self):
        """Return list of untriaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_untriaged') and not self.stats_tests_untriaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter_untriaged()
            .filter(build__revision=self)
        )

    @property
    def has_objects_missing_triage(self):
        """Return if there are failures missing triage."""
        try:
            return (
                self.stats_revision_untriaged or
                self.stats_builds_untriaged or
                self.stats_tests_untriaged
            )
        except AttributeError:
            return None

    @property
    def has_objects_with_issues(self):
        """Return if the revision or one of it's builds and tests has any issues."""
        try:
            return (
                self.stats_revision_triaged or
                self.stats_builds_triaged or
                self.stats_tests_triaged
            )
        except AttributeError:
            return None

    @property
    def nvr(self):
        """
        Return kernel NVR.

        Look for the pipeline linked to this revision and get the kernel_version.
        """
        mergerun = self.mergerun_set.first()

        if not mergerun:
            return None

        return mergerun.pipeline.kernel_version

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBRevision from kcidb json."""
        if cls.objects.filter(id=data['id'], origin__name=data['origin']).exists():
            raise AlreadySubmitted(f'Entry with {data["origin"]} {data["id"]} already exists.')

        misc = data.get('misc', {})

        log_url = data.get('log_url')
        log = dw_models.Artifact.create_from_url(log_url) if log_url else None

        tree_name = data.get('tree_name')
        tree = dw_models.GitTree.create_from_string(tree_name) if tree_name else None

        origin = KCIDBOrigin.create_from_string(data['origin'])
        revision = cls.objects.create(
            id=data['id'],
            origin=origin,
            tree=tree,
            git_repository_url=data.get('git_repository_url'),
            git_repository_branch=data.get('git_repository_branch'),
            git_commit_hash=data.get('git_commit_hash'),
            git_commit_name=data.get('git_commit_name'),
            message_id=data.get('message_id'),
            description=data.get('description'),
            publishing_time=data.get('publishing_time'),
            discovery_time=data.get('discovery_time'),
            valid=data.get('valid'),
            log=log,
        )

        for contact in data.get('contacts', []):
            maintainer = Maintainer.create_from_address(contact)
            revision.contacts.add(maintainer)

        # Create the patch instances depending on the cki_pipeline_type.
        patches_data = data.get('patch_mboxes', [])
        if patches_data:
            patch_urls = [patch['url'] for patch in patches_data]
            if get_nested_key(misc, 'pipeline/variables/cki_pipeline_type') == 'patchwork':
                patches = dw_models.PatchworkPatch.create_from_urls(patch_urls)
            else:
                patches = dw_models.Patch.create_from_urls(patch_urls)
            revision.patches.set(patches)

        if is_cki_submission(data):
            dw_models.MergeRun.create_from_misc(revision, misc)

        signals.kcidb_object.send(
            sender='kcidb.revision.create_from_json',
            status='new',
            object_type='revision',
            objects=[revision],
        )

        return revision


class KCIDBBuildManager(models.Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBBuild."""

    def aggregated(self):
        """Add aggregated information."""
        tests_run = KCIDBTest.objects.filter(build__id=models.OuterRef('id'))
        tests_pass = tests_run.filter(status__name='PASS')
        tests_skip = tests_run.filter(status__name='SKIP')
        tests_fail = tests_run.filter(status__name='FAIL', waived=False)

        counts = {
            'tests_run': tests_run,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
        }

        annotations = {
            'stats_tests_passed': ~models.Exists(tests_fail),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('id').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_untriaged(self):
        """Filter untriaged builds."""
        return self.filter(
            valid=False,
            issues=None
        )


class KCIDBBuild(models.Model):
    """Model for KCIDBBuild."""

    iid = models.AutoField(primary_key=True)

    revision = models.ForeignKey('KCIDBRevision', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    description = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    architecture = models.ForeignKey('Architecture', on_delete=models.PROTECT, null=True, blank=True)
    command = models.TextField(null=True, blank=True)
    compiler = models.ForeignKey('Compiler', on_delete=models.PROTECT, null=True, blank=True)
    input_files = models.ManyToManyField('Artifact', related_name='build_input', blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='build_output', blank=True)
    config_name = models.CharField(max_length=20, null=True, blank=True)
    config_url = models.URLField(null=True, blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.PROTECT, null=True, blank=True)
    valid = models.BooleanField(null=True)

    issues = models.ManyToManyField('Issue')

    objects = KCIDBBuildManager()

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    @property
    def is_public(self):
        """
        Return True if this KCIDBBuild is public.

        If there's no BuildRun associated, or any of the BuildRuns
        associated with this KCIDBBuild has a kernel_type
        different to 'upstream', consider it private.
        """
        return (
            self.buildrun_set.exists() and
            not self.buildrun_set.exclude(
                pipeline__variables__key='kernel_type',
                pipeline__variables__value='upstream'
            ).exists()
        )

    @property
    def tests_passed(self):
        """Return True if all tests passed."""
        try:
            return not bool(self.stats_tests_fail_count)
        except AttributeError:
            return not self.kcidbtest_set.filter(status__name='FAIL', waived=False).exists()

    @property
    def all_passed(self):
        """Return True if the build and tests passed."""
        return self.valid and self.tests_passed

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBBuild from kcidb json."""
        if cls.objects.filter(id=data['id'], origin__name=data['origin']).exists():
            raise AlreadySubmitted(f'Entry with {data["origin"]} {data["id"]} already exists.')

        misc = data.get('misc', {})

        arch = data.get('architecture')
        arch = dw_models.Architecture.objects.get_or_create(name=arch)[0] if arch else None

        compiler = data.get('compiler')
        compiler = dw_models.Compiler.objects.get_or_create(name=compiler)[0] if compiler else None

        log = data.get('log_url')
        log = dw_models.Artifact.create_from_url(log) if log else None

        try:
            revision = KCIDBRevision.objects.get(id=data['revision_id'], origin__name=data['origin'])
        except KCIDBRevision.DoesNotExist as exc:
            raise MissingParent('Parent Revision is not present on the DB') from exc

        origin = KCIDBOrigin.create_from_string(data['origin'])
        build = cls.objects.create(
            revision=revision,
            id=data['id'],
            origin=origin,
            description=data.get('description'),
            start_time=data.get('start_time'),
            duration=data.get('duration'),
            architecture=arch,
            command=data.get('command'),
            compiler=compiler,
            config_name=data.get('config_name'),
            config_url=data.get('config_url'),
            log=log,
            valid=data.get('valid'),
        )

        for input_file in data.get('input_files', []):
            file = dw_models.Artifact.objects.create(
                name=input_file['name'],
                url=input_file['url'],
            )
            build.input_files.add(file)

        for output_file in data.get('output_files', []):
            file = dw_models.Artifact.objects.create(
                name=output_file['name'],
                url=output_file['url'],
            )
            build.output_files.add(file)

        if is_cki_submission(data):
            dw_models.BuildRun.create_from_misc(build, misc)

        signals.kcidb_object.send(
            sender='kcidb.build.create_from_json',
            status='new',
            object_type='build',
            objects=[build],
        )

        return build


class KCIDBTestManager(models.Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBTestManager."""

    def filter_untriaged(self):
        """Filter untriaged tests."""
        return self.filter(
            status__name__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
            issues=None
        )


class KCIDBTest(models.Model):
    """Model for KCIDBTest."""

    iid = models.AutoField(primary_key=True)

    build = models.ForeignKey('KCIDBBuild', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    environment = models.ForeignKey('BeakerResource', on_delete=models.PROTECT, null=True, blank=True)
    test = models.ForeignKey('Test', on_delete=models.PROTECT, null=True, blank=True)
    status = models.ForeignKey('TestResult', on_delete=models.PROTECT, null=True, blank=True)
    waived = models.BooleanField(null=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test', blank=True)

    issues = models.ManyToManyField('Issue')

    objects = KCIDBTestManager()

    UNSUCCESSFUL_STATUSES = ('ERROR', 'FAIL')

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    @property
    def is_public(self):
        """
        Return True if this KCIDBTest is public.

        If there's no TestRun associated, or any of the TestRuns
        associated with this KCIDBTest has a kernel_type
        different to 'upstream', consider it private.
        """
        return (
            self.testrun_set.exists() and
            not self.testrun_set.exclude(
                pipeline__variables__key='kernel_type',
                pipeline__variables__value='upstream'
            ).exists()
        )

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBTest from kcidb json."""
        if cls.objects.filter(id=data['id'], origin__name=data['origin']).exists():
            raise AlreadySubmitted(f'Entry with {data["origin"]} {data["id"]} already exists.')

        misc = data.get('misc', {})

        try:
            build = KCIDBBuild.objects.get(id=data['build_id'])
        except KCIDBBuild.DoesNotExist as exc:
            raise MissingParent('Parent Build is not present on the DB') from exc

        environment = data.get('environment')
        environment = dw_models.BeakerResource.objects.get_or_create(
            fqdn=environment['description']
        )[0] if environment else None

        test_path = data.get('path')
        test_description = data.get('description')
        if test_description:
            test = dw_models.Test.objects.update_or_create(
                name=test_description,
                defaults={
                    'universal_id': test_path,
                }
            )[0]
        else:
            test = None

        status = data.get('status')
        status = dw_models.TestResult.objects.get_or_create(name=status)[0] if status else None

        origin = KCIDBOrigin.create_from_string(data['origin'])
        test = cls.objects.create(
            build=build,
            id=data['id'],
            origin=origin,
            environment=environment,
            test=test,
            status=status,
            waived=data.get('waived'),
            start_time=data.get('start_time'),
            duration=data.get('duration'),
        )

        for output_file in data.get('output_files', []):
            file = dw_models.Artifact.objects.create(
                name=output_file['name'],
                url=output_file['url'],
            )
            test.output_files.add(file)

        if is_cki_submission(data):
            if 'beaker' in misc.keys():
                dw_models.BeakerTestRun.create_from_misc(test, misc)
            else:
                dw_models.TestRun.create_from_misc(test, misc)

        signals.kcidb_object.send(
            sender='kcidb.test.create_from_json',
            status='new',
            object_type='test',
            objects=[test],
        )

        return test

# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Patch models file."""
from django.db import models

# Patches stuff
from datawarehouse.patches import store_patchwork_series_patches
from datawarehouse.utils import parse_patches_from_urls

from . import pipeline_models


class PatchManager(models.Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, url, subject):
        """Lookup the object by the natural key."""
        return self.get(url=url, subject=subject)


class Patch(models.Model):
    """Model for Patch."""

    url = models.CharField(max_length=500)
    subject = models.CharField(max_length=200)
    pipelines = models.ManyToManyField(pipeline_models.Pipeline, related_name='patches')

    objects = PatchManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.url}'

    def natural_key(self):
        """Return the natural key."""
        return (self.url, self.subject)

    class Meta:
        """Metadata."""

        ordering = ('id',)
        unique_together = ('url', 'subject')

    @property
    def gui_url(self):
        """Return a user-usable URL.

        Try to get the web_url from PatchworkPatch, but fall back to the url for
        a plain patch. When iterating, it might be more efficient to prefetch
        with select_related('patchworkpatch').

        This only works for saved models.
        """
        return (self.patchworkpatch.web_url  # pylint: disable=no-member
                if hasattr(self, 'patchworkpatch') else self.url)

    @classmethod
    def create_from_urls(cls, urls):
        """Create Patch instances from list of urls."""
        return [
            cls.objects.get_or_create(**patch)[0]
            for patch in parse_patches_from_urls(urls)
        ]


class PatchworkSubmitter(models.Model):
    """Model for PatchworkSubmitter."""

    name = models.CharField(max_length=50)
    email = models.EmailField()
    submitter_id = models.IntegerField(unique=True)  # from Patchwork

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'


class PatchworkProject(models.Model):
    """Model for PatchworkProject."""

    project_id = models.IntegerField(unique=True)  # from Patchwork
    name = models.CharField(max_length=20)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'


class PatchworkSeries(models.Model):
    """Model for PatchworkSeries."""

    series_id = models.IntegerField(unique=True)  # from Patchwork
    url = models.URLField()
    web_url = models.URLField()
    name = models.CharField(max_length=200)
    submitter = models.ForeignKey(PatchworkSubmitter,
                                  on_delete=models.CASCADE,
                                  related_name='patch_series')
    project = models.ForeignKey(PatchworkProject, on_delete=models.CASCADE)
    submitted_date = models.DateTimeField()
    skipped = models.BooleanField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.series_id}: {self.name}'

    @property
    def pipelines(self):
        """Return a slightly optimized list of related pipelines."""
        patch = self.patches.all()[:1]
        return (pipeline_models.Pipeline.objects
                .filter(patches=patch)
                .add_stats()
                .select_related('gittree'))

    class Meta:
        """Metadata."""

        ordering = ('-series_id',)


class PatchworkPatch(Patch):
    """Model for PatchworkPatch."""

    series = models.ManyToManyField(PatchworkSeries, related_name='patches')
    web_url = models.URLField()
    msgid = models.CharField(max_length=255)
    mbox = models.CharField(max_length=255)
    patch_id = models.IntegerField(unique=True)  # from Patchwork

    @classmethod
    def create_from_urls(cls, urls):
        """Create Patch instances from list of urls."""
        return store_patchwork_series_patches(urls)

"""
Datawarehouse authorization layer.

Define whether a user has permissions to access certain data.
"""

from datawarehouse import models


class GitTreeAuthorizationBackend:
    """
    GitTree based authorization backend.

    Limit user access to GitTree related data based on the authorizations it is related to.
    """

    @staticmethod
    def get_authorizations(user):
        """Get the list of gittree authorizations the user has."""
        gittree_authorizations = {'read': set(), 'write': set()}
        user_groups = user.groups.all()

        for method in ('read', 'write'):

            # Add the trees with anonymous access.
            gittree_authorizations[method].update(
                models.GitTree.objects
                .exclude(policy=None)
                .filter(**{f'policy__{method}_group': None})
                .values_list('id', flat=True)
            )

            # Add the trees where the user has access.
            for group in user_groups:
                gittree_authorizations[method].update(
                    getattr(group, f'{method}_policies')
                    .values_list('gittree__id', flat=True)
                )

        # Sets are not JSON serializable, convert to list.
        for key in gittree_authorizations:
            gittree_authorizations[key] = sorted(list(gittree_authorizations[key]))

        return gittree_authorizations

    @staticmethod
    def _is_authorized(request, gittree, method):
        """Check if the request is authorized for the gittree specified."""
        user_authorizations = request.session['gittree_authorizations'][method]
        return gittree.name in user_authorizations

    @staticmethod
    def is_read_authorized(request, gittree):
        """Check if the user authorized to read this GitTree."""
        return GitTreeAuthorizationBackend._is_authorized(request, gittree, 'read')

    @staticmethod
    def is_write_authorized(request, gittree):
        """Check if the user authorized to write this GitTree."""
        return GitTreeAuthorizationBackend._is_authorized(request, gittree, 'write')


class RequestAuthorization:
    """
    RequestAuthorization middleware.

    Inject user authorization data into the session.
    """

    def __init__(self, get_response):
        """Initialize middleware."""
        self.get_response = get_response

    @staticmethod
    def fill_gittree_authorizations(request):
        """Add gittree_authorizations data to the session."""
        request.session.update(
            {
                'gittree_authorizations': GitTreeAuthorizationBackend.get_authorizations(request.user)
            }
        )

    @staticmethod
    def fill_user_data(request):
        """
        Update session to store user data.

        Django keeps the session after the user logs in/out, so we need to check that
        the data stored corresponds to the request user.

        If the request user is not the same as the stored data, update the data.
        """
        user_id = request.user.id or 'anonymous'
        session_user_id = request.session.get('user_id')

        if session_user_id != user_id:
            request.session['user_id'] = user_id
            RequestAuthorization.fill_gittree_authorizations(request)

    def __call__(self, request):
        """
        Override __call__ method.

        Before calling get_response, fill the gittree_authorization data.
        """
        self.fill_user_data(request)
        response = self.get_response(request)
        return response

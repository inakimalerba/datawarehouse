# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Urls file."""
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.views.generic.base import RedirectView

from datawarehouse import views
from datawarehouse import views_kcidb

urlpatterns = [
    path('', RedirectView.as_view(url='dashboard', permanent=False)),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    path('confidence/<str:group>', views.confidence,),
    path('cron', views.cron_run,),
    path('dashboard', views.dashboard, name='Dashboard'),
    path('details/<str:group>/<int:item_id>', views.details,),
    path('issue/list/<str:group>', views.issue_list, name='issue_list'),
    path('issue/list', RedirectView.as_view(url='list/unresolved', permanent=False)),
    path('issue/regex', views.issue_regex, name='issue_regex'),
    path('issue', views.issue_new_or_edit,),
    path('issue/<int:issue_id>/resolve', views.issue_resolve,),
    path('patches/submitter/<str:group>', views.patch_get_by_submitter),
    path('patches/summary/<str:group>', views.patch_summary),
    path('patches/submitter', RedirectView.as_view(url='submitter/tested', permanent=False, query_string=True)),
    path('patches', RedirectView.as_view(url='patches/summary/tested', permanent=False)),
    path('pipeline/summary', RedirectView.as_view(url='summary/all', permanent=False)),
    path('pipeline/summary/<str:tree>', views.pipeline_summary),
    path('pipeline/failures/<str:group>', views.pipelines_by_failure),
    path('pipeline/failures', RedirectView.as_view(url='failures/all', permanent=False)),
    path('pipeline/<int:pipeline_id>', views.pipeline_get, name='pipeline_get'),
    path('pipeline/submitter', views.pipelines_get_by_submitter),
    path('pipeline/running', views.pipelines_running,),
    path('metrics', RedirectView.as_view(url='metrics/all', permanent=False)),
    path('metrics/<str:tree>', views.metrics,),

    path('kcidb/failures', RedirectView.as_view(url='failures/all', permanent=False)),
    path('kcidb/failures/<str:stage>',
         views_kcidb.revisions_list_by_failure, name='views.kcidb.failures'),
    path('kcidb/issues/occurrences', views_kcidb.kcidb_issue, name='views.kcidb.issues'),
    path('kcidb/revisions',
         views_kcidb.revisions_list, name='views.kcidb.revisions'),
    path('kcidb/revisions/<int:revision_iid>',
         views_kcidb.revisions_get, name='views.kcidb.revisions'),
    path('kcidb/builds/<int:build_iid>',
         views_kcidb.builds_get, name='views.kcidb.builds'),
    path('kcidb/tests/<int:test_iid>',
         views_kcidb.tests_get, name='views.kcidb.tests'),
    path('api/', include('datawarehouse.api.urls')),
    path('__debug__/', include(debug_toolbar.urls)),
] \
    + static(settings.MEDIA_PATH, document_root=settings.MEDIA_ROOT) \
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Web development guidelines/tips

Most of these are subjective! They are mostly provided to give a sense
of direction and to try to keep things consistent. If you are doing
things in a different way and decide that's ok, please do update this
document to reflect how you want things to be done!

## CSS

* CSS class names should be kebab-case (`like-this`).
* Ideally, those classes would have semantic names (more like
  "failed-build" than "red-border").
* CSS can be loads of stuff, including adding text (:after
  pseudo-selector and the "content" attribute), adding icons (by
  adding extra eg. right padding and setting an image as background,
  centered and to the right), doing simple text transformations
  (text-transform), etc.
* It's often better to keep all visual stuff in markup/CSS instead of
  part in markup/CSS and part on the backend, in Python.

## JavaScript

* JavaScript variables and functions should be named camelCased (`likeThis`).
* Generic JS should be put in static/js/custom.js, while JS for a
  single template should be in the template itself (but be careful
  with duplicated code, if that template is to be included more than
  once).
* For the information we are showing to anonymous users it would be
  nice to not require JS at all. And in general, a lot of custom JS is
  probably not a good idea for this kind of application (= Django,
  server-side rendering).
* I would avoid using JS to adapt HTML interfaces, eg. to adapt a
  single edit dialog to the element about to be edited. Instead, I
  would rather have several copies of the dialog, one for each element
  to be edited. See FASTMOVING-1956 for a bit of context, and the
  rationale below!

## HTML

* Element names and ids should be named camelCased (I would argue it
  would be more natural to do them kebab-cased, but it's not super
  important and the code already uses camelCase convention, so...).
* Being "semantic" is usually a good idea: wrapping all navigation
  elements in a <nav>, using <section>, <main>, <ul> or <ol> for
  lists, even if they are not going to *look* like lists, etc.

## Multiple dialog rationale

This is a hairy issue and it has advantages and disadvantages. First,
advantages:

* Less custom JS: to open the dialog we just use a generic way to open
  dialogs (in the current case, those special attributes bootstrap
  uses to handle dialogs).
* Less chance to break things when changing the name of a field. The
  reason is that the JS will live in custom.js, but the markup with
  the field names will be in some template. There is no explicit "API"
  or anything, so it's hard to know if you're using the right field
  names.
* If the dialog can be used for different things (eg. create and
  edit), it's better to have the variants already built in server-side
  templates, instead of adapting markup dynamically. The latter is a
  pain, it's brittle, and it's likely to yield suboptimal results
  because we will tend to do the minimum necessary (because it takes
  effort).

Now, the disadvantages:

* We end up with a lot more HTML! In some cases this could represent a
  sizable chunk of data that we might want to avoid.
* Subjectively, it's ugly, because we are repeating lots of markup
  instead of sending it just once.

*If* the whole application was written in JS (something we probably
don't want, at least now) this wouldn't be a problem at all.

## Resources

Some links that might be useful:

* [HTML and CSS simple tricks for your website](https://thomasorus.com/html-tips.html):
  covers semantic HTML, accessibility, layouts in CSS, etc.
* [Will it CORS?](https://httptoolkit.tech/will-it-cors/): it's a
  tool to understand when to use CORS and how.

# Issues

**Note:** All links on this page point to the CKI internal DW instance.

DataWarehouse provides a way to tag failures on Revisions, Builds and Tests.

## Glossary

Issue
:   A certain anomaly is represented on the DataWarehouse with an Issue entry.
    Each Issue represents a problem that can be found across different pipelines.

    Example: 'Timeout while doing X task'

Issue Kind
:   Issues are grouped on Issue Kinds. Each kind represents a different category
    of failures.

    Example: 'Infrastructure Issue', 'Kernel Bug'

Issue Occurrence
:   The linking relationship between Issues and KCIDB Objects are called Issue Occurrence

![Issue Design](issues_design.svg)

## Issue Analysis

The [Data Warehouse] can be used to view/track issues, in addition to viewing recent testing trends per kernel build.

### [Issues List](https://datawarehouse.internal.cki-project.org/issue/list/unresolved)

#### View Known Issues

Known issues are tracked in the datawarehouse.
These are grouped into categories such as `Kernel Bug`, `Unstable Test`, `Infrastructure Problem`, etc.
Issues can be tagged as resolved, which represents that an issue is no longer present.
In addition to this, `Kernel Bug` issues have an `Origin Tree` property which points to the first tree where the failure was found.

On the [Issues List] page (On the left sidebar: Issues → Issues List) it's possible to find the list of failures, grouped by resolution status, and the revisions against which they are tagged against.

#### Edit Known Issues

Using the [Issues List] page it's possible to modify the Issues.

* To update Issue resolution: Select Actions → Mark as Resolved/Unresolved

* To update Issue kind, url, description or origin: Select Actions → Edit Issue

### Tag a new Issue

Each revision which includes failures should be triaged and tagged with the appropriate Issue. To tag a new Issue, please follow these steps:

1. Link a new failure by adding an Issue Occurrence.
    1. View the [Failed Revisions] by selecting Revisions → Failures on the sidebar. You can filter by Revision, Build or Test failures by selecting the tabs on top of the list.
    1. View/open failed builds or tests to debug the issue.
    1. Select Actions → Associate Issue.
    1. Select the Issue linked to the failure. The textbox can be used to filter the dropdown options.
    1. If there is no Issue registered for the failure you found, create a new one by clicking on 'Create new issue'.
    1. Select the affected revision, build or test and select 'Save' near the bottom.
1. Add a regex pattern to have our bot triager tag the known issue for new revisions.
    1. Open the [Issues Regexes] page by selecting Issues → Issues Regexes on the sidebar.
    1. Select Actions → New Issue Regex
    1. Select the Issue you just filed from the drop down menu
    1. Fill out related details for the regex pattern including Text Match (regex pattern), Text Name Match, and File Name Match

### Testing Trends

Testing trends are available in the [Data Warehouse] to view test results per kernel build, machines they are run against, and test confidence rating (test stability).

#### Tests Confidence

Confidence graphs include a summary of the test results overall (Pass|Fail|Error|Skip), this helps us determine how often a test is failing in the pipeline and if any follow up action is needed.
The [Tests Confidence] page can be found under Confidence → Tests on the sidebar.

#### Test Results

You can view test results for all kernels and machines they were run against by selecting the test from the Test Confidence page to drill down further.

You have the option to filter the results by status at the top of the page, this helps determine if a test has failed a previous kernel build or it's related to the kernel patch/build under test.

[Data Warehouse]: https://datawarehouse.internal.cki-project.org
[Issues List]: https://datawarehouse.internal.cki-project.org/issue/list/unresolved
[Failed Revisions]: https://datawarehouse.internal.cki-project.org/kcidb/failures/all
[Issues Regexes]: https://datawarehouse.internal.cki-project.org/issue/regex
[Tests Confidence]: https://datawarehouse.internal.cki-project.org/confidence/tests

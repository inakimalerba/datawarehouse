# Report

A report represents a email sent by the reporter.

## Create

Create a Report on certain pipeline.

`POST /api/1/pipeline/$pipeline_id/report`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `content` | `mbox` | Yes | Mbox of the email. |

## Get by Pipeline

Get a list of reports for a pipeline.

`GET /api/1/pipeline/$pipeline_id/report`

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": {
        "reports": [
            {
                "id": 7,
                "addr_to": [
                    {
                        "email": "email@redhat.com"
                    },
                    {
                        "email": "another@redhat.com"
                    }
                ],
                "addr_cc": [],
                "addr_bcc": [],
                "subject": "✅ PASS: Re: [PATCH] lorem ipsum",
                "body": "Email body.",
                "sent_at": "2020-01-14T18:37:23Z",
                "pipeline": 16898
            },
        ]
    }
}
```

## Get Missing

Get a list of pipelines without a report.

Note: Pipelines that finished less than 3 hours before the request
are not exposed on this endpoint to avoid race conditions with the report crawler.

`GET /api/1/report/missing`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `since` | `datetime` | No | Get pipelines newer that this date.|

Example of response:

```json
{
    "count": 1242,
    "next": "http://server/api/1/report/missing?limit=30&offset=30",
    "previous": null,
    "results": {
        "pipelines": [
            {
                "commit_id": "50c72fb1d9713c5f562b064ae5accc02c332e842",
                "project": {
                    "project_id": 16,
                    "path": "cki-project/brew-pipeline"
                },
                "web_url": "https://xci32.lab.eng.rdu2.redhat.com/cki-project/brew-pipeline/pipelines/442198",
                "pipeline_id": 442198,
                "created_at": "2020-02-17T08:38:36.172000Z",
                "started_at": "2020-02-17T08:38:39.481000Z",
                "finished_at": "2020-02-17T10:50:14.912000Z",
                "duration": 7889,
                "test_passed": true,
                "build_passed": true,
                "merge_passed": true,
                "lint_passed": true,
                "all_passed": true,
                "gittree": {
                    "name": "rhel7"
                },
                "kernel_version": "3.10.0-1126.2.el7.test.cki.net.bz1779533.src.rpm"
            },
          ...
        ]
    }
}
```

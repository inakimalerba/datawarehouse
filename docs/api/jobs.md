# Job

## List

Get a list of jobs from a pipeline's stage.

`GET /api/1/pipeline/$pipeline_id/jobs/$stage_name`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `pipeline_id` | `int` | Yes | ID of the pipeline. |
| `stage_name` | `str` | Yes | Name of the stage. `lint`, `merge`, `build` or `test` |

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": {
        "jobs": [
            {
                "jid": 794406,
                "success": true,
                "command": "/builds/cki-project/cki-pipeline/venv/bin/python3 /builds/cki-project/cki-pipeline/script.py"
            }
        ]
    }
}
```

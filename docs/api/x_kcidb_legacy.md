# KCIDB Legacy

This endpoint is kept for compatibility only.
**Do not use it.**

Get information about revisions, builds and tests formatted to comply with
the KCIDB schema.

`GET /api/1/kcidb/data/(revisions|builds|tests)`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `last_retrieved_id` | `int` | Yes | Only elements with internal database id bigger than this number will be retrieved. |
| `min_pipeline_started_at` | ISO Datetime | No | `YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]` Only elements corresponding with pipelines newer than this date will be retrieved. |

Example of response:

```json
{
    "count": "(int) total number of results",
    "next": "(str) URL of the next page",
    "previous": "(str) URL of the previous page",
    "results": {
        "data": {
            "version": "(str) schema version",
            "(revisions|builds|tests)": "(list) entries",
        },
        "last_retrieved_id": "(int) id of the last retrieved item. If no results on the page, `last_retrieved_id` parameter will be returned instead."
    }
}
```

## Content examples

```json
{
    "count": 95,
    "next": "{server}/api/1/kcidb/data/tests?last_retrieved_id=0",
    "previous": null,
    "results": {
        "data": {
            "version": "1",
            "tests": [
                {
                "build_origin": "redhat",
                "build_origin_id": "426535",
                "origin": "redhat",
                "origin_id": "102055725",
                "description": "Boot test",
                "status": "PASS",
                "waived": false
                }
            ]
        },
        "last_retrieved_id": 441966
    }
}
```

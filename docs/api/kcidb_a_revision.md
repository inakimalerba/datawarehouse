# KCIDB - Revision

## Get

Get a single revision.

`GET /api/1/kcidb/revisions/$revision_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `revision_id` | `str/int` | Yes | `id` or `iid` of the revision to get. |

Example of response:

```json
{
    "id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
    "origin": "redhat",
    "tree_name": "upstream",
    "git_repository_url": "https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
    "git_repository_branch": "queue/5.7",
    "git_commit_hash": "b8fba93561c984f336d47d6d544be3a2a920bac3",
    "publishing_time": "2020-06-30T17:06:26.357000Z",
    "discovery_time": "2020-06-30T17:06:26.357000Z",
    "valid": true,
    "misc": {
        "iid": 137
    }
}
```

## List

Get a list of revisions.

`GET /api/1/kcidb/revisions`

Example of response:

```json
{
    "count": 168,
    "next": "http://server/api/1/kcidb/revisions?limit=30&offset=30",
    "previous": null,
    "results": [
        {
            "id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "origin": "redhat",
            "tree_name": "upstream",
            "git_repository_url": "https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
            "git_repository_branch": "queue/5.7",
            "git_commit_hash": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "publishing_time": "2020-06-30T17:06:26.357000Z",
            "discovery_time": "2020-06-30T17:06:26.357000Z",
            "valid": true,
            "misc": {
                "iid": 137
            }
        },
        ...
    ]
}
```

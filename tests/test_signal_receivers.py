"""Test the signal_receivers module."""
from unittest import mock

from django import test
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse import signal_receivers
from datawarehouse import signals
from datawarehouse.api.kcidb import serializers as kcidb_serializers


class SignalsTest(test.TestCase):
    """Unit tests for the Signals module."""

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_message(send_kcidb):
        """Test that the kcidb message is sent correctly."""
        rev = models.KCIDBRevision.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        signal_receivers.send_kcidb_object_message(
            status='some-status',
            object_type='revision',
            objects=[rev],
        )
        send_kcidb.assert_called_with([{
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'some-status',
            'object_type': 'revision',
            'object': kcidb_serializers.KCIDBRevisionSerializer(rev).data,
            'id': rev.id,
            'iid': rev.iid,
        }])

    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification', mock.Mock())
    def test_signal_kcidb_object(self):
        """Test kcidb_object signal receivers."""
        rev = models.KCIDBRevision.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
        )
        receivers = signals.kcidb_object.send(
            sender='test', status='test', object_type='revision', objects=[rev]
        )
        self.assertEqual(
            [(signal_receivers.send_kcidb_object_message, None)],
            receivers
        )

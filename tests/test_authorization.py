"""Test authorization module."""
from unittest import mock

import django.contrib.auth.models as auth_models

from datawarehouse import authorization
from datawarehouse import models
from tests import utils


class TestGitTreeAuthorizationBackend(utils.TestCase):
    """Test GitTreeAuthorizationBackend authorization module."""

    @staticmethod
    def _populate(data):
        """Populate test data."""
        auth_models.Group.objects.bulk_create([
            auth_models.Group(name=group_name)
            for group_name in data['groups']
        ])

        models.GitTreePolicy.objects.bulk_create([
            models.GitTreePolicy(
                name=policy_name,
                read_group=auth_models.Group.objects.get(name=group_names['read']) if group_names['read'] else None,
                write_group=auth_models.Group.objects.get(name=group_names['write']) if group_names['write'] else None,
            ) for policy_name, group_names in data['gittrees_policies'].items()
        ])

        models.GitTree.objects.bulk_create([
            models.GitTree(
                name=gittree_name,
                policy=(
                    models.GitTreePolicy.objects.get(name=policy_name)
                    if policy_name else None
                )
            ) for gittree_name, policy_name in data['gittrees'].items()
        ])

        for username, groups in data['users'].items():
            user = auth_models.User.objects.create(
                username=username,
            )
            for group_name in groups:
                group = auth_models.Group.objects.get(name=group_name)
                user.groups.add(group)

    def test_get_authorizations(self):
        """
        Test get_authorizations.

        Run get_authorizations aginst different users, belonging to different groups
        containing policies, and check that the read and write trees returned
        are correct.
        """
        data = {
            'groups': [
                'group_1',
                'group_2',
            ],
            'gittrees_policies': {
                'policy-r1-wn': {'read': 'group_1', 'write': None},
                'policy-rn-w2': {'read': None, 'write': 'group_2'},
                'policy-r2-w1': {'read': 'group_2', 'write': 'group_1'},
                'policy-rn-wn': {'read': None, 'write': None},
            },
            'gittrees': {
                'tree_1': 'policy-r1-wn',
                'tree_2': 'policy-rn-w2',
                'tree_3': 'policy-r2-w1',
                'tree_4': 'policy-rn-wn',
                'tree_5': None,
            },
            'users': {
                'user_1': ['group_1'],
                'user_2': ['group_2'],
                'user_3': ['group_1', 'group_2'],
                'user_4': [],
            }
        }

        self._populate(data)

        test_cases = {
            'user_1': {
                'read': ['tree_1', 'tree_2', 'tree_4'],
                'write': ['tree_1', 'tree_3', 'tree_4']
            },
            'user_2': {
                'read': ['tree_2', 'tree_3', 'tree_4'],
                'write': ['tree_1', 'tree_2', 'tree_4']
            },
            'user_3': {
                'read': ['tree_1', 'tree_2', 'tree_3', 'tree_4'],
                'write': ['tree_1', 'tree_2', 'tree_3', 'tree_4']
            },
            'user_4': {
                'read': ['tree_2', 'tree_4'],
                'write': ['tree_1', 'tree_4']
            },
            # AnonymousUser
            None: {
                'read': ['tree_2', 'tree_4'],
                'write': ['tree_1', 'tree_4']
            }
        }

        for username, policy in test_cases.items():
            if username:
                user = auth_models.User.objects.get(username=username)
            else:
                user = auth_models.AnonymousUser()

            # For performance reasons, get_authorizations returns the ids of the trees.
            # Convert tree names into ids.
            self.assertDictEqual(
                authorization.GitTreeAuthorizationBackend.get_authorizations(user),
                {
                    'read': list(
                        models.GitTree.objects.filter(name__in=policy['read'])
                        .values_list('id', flat=True),
                    ),
                    'write': list(
                        models.GitTree.objects.filter(name__in=policy['write'])
                        .values_list('id', flat=True),
                    )
                }
            )

    def test_is_authorized_method(self):
        """
        Test _is_authorized.

        Check that _is_authorized looks for the correct key on the gittrees_authorization
        session data.

        Given 2 different gittrees and 2 authorizations, give the user a mix of them and
        check that it can read one and write the other.
        """
        data = {
            'groups': [],
            'gittrees_policies': {},
            'gittrees': {
                'gittree_1': None,
                'gittree_2': None,
            },
            'users': {},
        }
        self._populate(data)

        session = self.client.session
        session['gittree_authorizations'] = {'read': ['gittree_1'], 'write': ['gittree_2']}
        session.save()

        cases = (
            ('gittree_1', 'read', True),
            ('gittree_1', 'write', False),
            ('gittree_2', 'read', False),
            ('gittree_2', 'write', True),
        )

        for gittree_name, method, authorized in cases:
            gittree = models.GitTree.objects.get(name=gittree_name)
            # pylint: disable=protected-access
            self.assertEqual(
                authorization.GitTreeAuthorizationBackend._is_authorized(self.client, gittree, method),
                authorized,
                f'{gittree_name} - {method} - {authorized}',
            )

        # pylint: disable=protected-access
        # Ensure that an undefined method raises an exception.
        self.assertRaises(
            Exception,
            authorization.GitTreeAuthorizationBackend._is_authorized, self.client, gittree, 'foobar'
        )

    @staticmethod
    @mock.patch('datawarehouse.authorization.GitTreeAuthorizationBackend._is_authorized')
    def test_is_read_authorized(is_authorized):
        """Test is_read_authorized calls _is_authorized correctly."""
        request = mock.Mock()
        gittree = mock.Mock()
        authorization.GitTreeAuthorizationBackend.is_read_authorized(request, gittree)
        is_authorized.assert_called_with(request, gittree, 'read')

    @staticmethod
    @mock.patch('datawarehouse.authorization.GitTreeAuthorizationBackend._is_authorized')
    def test_is_write_authorized(is_authorized):
        """Test is_write_authorized calls _is_authorized correctly."""
        request = mock.Mock()
        gittree = mock.Mock()
        authorization.GitTreeAuthorizationBackend.is_write_authorized(request, gittree)
        is_authorized.assert_called_with(request, gittree, 'write')


class TestRequestAuthorization(utils.TestCase):
    """Test RequestAuthorization middleware."""

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_user_data')
    def test_call(self, fill_user_data):
        """
        Test __call__ calls to fill_user_data *before* calling get_response.

        Check that the request parameter for get_response call contains the
        modified data by fill_user_data.
        """
        def _fill_user_data_mock(request):
            request.session['dummy_data'] = {'foo': 'bar'}

        class GetResponseMock:
            # pylint: disable=too-few-public-methods
            """Mock get_response."""

            def __init__(self, testcase):
                """Save parent self to call assertEqual."""
                self.testcase = testcase

            def __call__(self, request):
                """Look for the dummy_data in the session."""
                self.testcase.assertEqual(
                    request.session['dummy_data'],
                    {'foo': 'bar'}
                )

        fill_user_data.side_effect = _fill_user_data_mock

        request = mock.Mock()
        request.session = {}

        authorization.RequestAuthorization(GetResponseMock(self))(request)
        self.assertTrue(fill_user_data.called)
        fill_user_data.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.GitTreeAuthorizationBackend.get_authorizations')
    def test_fill_gittree_authorizations(self, get_authorizations):
        """Test fill_gittree_authorizations adds the data to the session."""
        auths_mock = {'read': ['foo'], 'write': ['bar']}
        get_authorizations.return_value = auths_mock
        request = mock.Mock()
        request.session = {}

        authorization.RequestAuthorization.fill_gittree_authorizations(request)
        self.assertEqual(request.session, {'gittree_authorizations': auths_mock})

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_gittree_authorizations')
    def test_fill_user_data_first_request(self, fill_gittree_authorizations):
        """
        Test that fill_user_data updates the information on log in/out.

        First anonymous request updates user_id and calls fill_gittree_authorizations.
        """
        request = mock.Mock()
        request.session = {}

        anonymous = auth_models.AnonymousUser()

        request.user = anonymous
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual('anonymous', request.session.get('user_id'))
        fill_gittree_authorizations.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_gittree_authorizations')
    def test_fill_user_data_next_anon_request(self, fill_gittree_authorizations):
        """
        Test that fill_user_data updates the information on log in/out.

        Following anonymous request don't call fill_gittree_authorizations.
        """
        anonymous = auth_models.AnonymousUser()

        request = mock.Mock()
        request.user = anonymous
        request.session = {}
        request.session['user_id'] = 'anonymous'

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(fill_gittree_authorizations.called)

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_gittree_authorizations')
    def test_fill_user_data_login(self, fill_gittree_authorizations):
        """
        Test that fill_user_data updates the information on log in/out.

        When AnonymousUser logs in, user_id is updated and fill_gittree_authorizations is called.
        """
        request = mock.Mock()
        request.session = {}

        user = auth_models.User.objects.create(username='user_foo')

        request.user = user
        request.session['user_id'] = 'anonymous'

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual(user.id, request.session.get('user_id'))
        fill_gittree_authorizations.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_gittree_authorizations')
    def test_fill_user_data_next_logged_in_request(self, fill_gittree_authorizations):
        """
        Test that fill_user_data updates the information on log in/out.

        Following logged in request don't call fill_gittree_authorizations.
        """
        user = auth_models.User.objects.create(username='user_foo')

        request = mock.Mock()
        request.user = user
        request.session = {}
        request.session['user_id'] = user.id

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(fill_gittree_authorizations.called)

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_gittree_authorizations')
    def test_fill_user_data_logout(self, fill_gittree_authorizations):
        """
        Test that fill_user_data updates the information on log in/out.

        User logs out and fill_gittree_authorizations is called.
        """
        request = mock.Mock()
        request.session = {}

        anonymous = auth_models.AnonymousUser()
        user = auth_models.User.objects.create(username='user_foo')

        request.user = anonymous
        request.session['user_id'] = user.id

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual('anonymous', request.session.get('user_id'))
        fill_gittree_authorizations.assert_called_with(request)

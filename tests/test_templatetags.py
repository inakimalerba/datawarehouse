"""Test templatags."""
from datawarehouse import models
from datawarehouse.templatetags import trim_queryset
from tests import utils


class TestTrimQueryset(utils.TestCase):
    """Test trim_queryset templatetags."""

    def test_trim_queryset(self):
        """Test trim_queryset templatetag."""
        models.TestResult.objects.bulk_create([
            models.TestResult(name=name)
            for name in range(15)
        ])

        queryset = models.TestResult.objects.all().order_by('id')

        # Test default length: 10
        trimmed = trim_queryset.trim_queryset(queryset)
        self.assertEqual(10, len(trimmed))
        self.assertEqual(5, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(10)]
        )

        # Test length as parameter
        trimmed = trim_queryset.trim_queryset(queryset, trim_at=5)
        self.assertEqual(5, len(trimmed))
        self.assertEqual(10, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(5)]
        )

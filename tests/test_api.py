"""Test the views module."""
import json

from django.contrib.auth.models import Permission  # pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from datawarehouse import models
from datawarehouse import serializers
from tests import utils


class TestAPITokenAuthentication(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Smoke test to keep token auth working."""

    revision_id = 'a17c8f36b72cc5422a1897ff057eddd2b62ebac2'

    def setUp(self):
        """Set Up."""
        models.KCIDBRevision.objects.create(
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
            id=self.revision_id,
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        permissions = Permission.objects.filter(
            codename__in=['add_kcidbrevision', 'change_kcidbrevision', 'delete_kcidbrevision']
        )

        test_user = User.objects.create(username='test', email='test@test.com')
        test_user.user_permissions.set(permissions)
        token = Token.objects.create(user=test_user)

        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_post(self):
        """Test POST request."""
        issue = models.Issue.objects.first()

        response = self.api_client.post(
            f'/api/1/kcidb/revisions/{self.revision_id}/issues',
            json.dumps({'issue_id': issue.id}), content_type="application/json")
        self.assertEqual(201, response.status_code)

    def test_delete(self):
        """Test DELETE request."""
        issue = models.Issue.objects.first()
        models.KCIDBRevision.objects.get(id=self.revision_id).issues.add(
            issue
        )

        response = self.api_client.delete(
            f'/api/1/kcidb/revisions/{self.revision_id}/issues/{issue.id}',
            content_type="application/json")
        self.assertEqual(204, response.status_code)


class ViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_trees = [
            models.GitTree.objects.create(name='Tree 1'),
            models.GitTree.objects.create(name='Tree 2'),
        ]
        self.pipelines = [
            models.Pipeline.objects.create(pipeline_id=1, project=self.project,
                                           gittree=self.git_trees[0], duration=1234),
            models.Pipeline.objects.create(pipeline_id=2, project=self.project,
                                           gittree=self.git_trees[1]),
        ]
        self.hosts = [
            models.BeakerResource.objects.create(id=1, fqdn='host_1'),
            models.BeakerResource.objects.create(id=2, fqdn='host_2'),
        ]
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.tests = [
            models.Test.objects.create(id=1, name='test_1'),
            models.Test.objects.create(id=2, name='test_2'),
        ]
        self.results = [
            models.TestResult.objects.create(name='PASS'),
            models.TestResult.objects.create(name='FAIL'),
        ]
        self.stage = models.Stage.objects.create(name='stage')
        self.jobs = [
            models.Job.objects.create(name='job_1', stage=self.stage),
            models.Job.objects.create(name='job_2', stage=self.stage),
            models.Job.objects.create(name='job_3', stage=self.stage),
            models.Job.objects.create(name='job_4', stage=self.stage),
            ]

        # pylint: disable=line-too-long
        tests_list = [
            (0, self.jobs[0], self.pipelines[0], self.tests[0], self.results[1], self.hosts[1]), # noqa
            (1, self.jobs[1], self.pipelines[0], self.tests[1], self.results[0], self.hosts[0]), # noqa
            (2, self.jobs[2], self.pipelines[1], self.tests[0], self.results[1], self.hosts[0]), # noqa
            (3, self.jobs[3], self.pipelines[1], self.tests[1], self.results[0], self.hosts[1]), # noqa
        ]

        self.test_runs = []
        for incremental_id, job, pipeline, test_, result, host in tests_list:
            test_run = models.BeakerTestRun.objects.create(
                job=job, jid=incremental_id, task_id=incremental_id, recipe_id=incremental_id,
                pipeline=pipeline, test=test_, result=result,
                waived=False, kernel_arch=self.kernel_arch, beaker_resource=host, retcode=0
            )
            self.test_runs.append(test_run)

        models.LintRun.objects.create(job=self.jobs[0], jid=0, command='', pipeline=self.pipelines[0], success=False)
        models.MergeRun.objects.create(job=self.jobs[1], jid=1, pipeline=self.pipelines[1], success=False)

    def test_pipeline_get_json_not_found(self):
        """Test get_pipeline json. Pipeline not found."""
        response = self.client.get('/api/1/pipeline/0')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_pipeline_get_json(self):
        """Test get_pipeline json."""
        pipeline = models.Pipeline.objects.first()

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.PipelineSerializer(pipeline).data,
            response.json()
        )

    def test_pipeline_get_json_reporter(self):
        """Test get_pipeline json. Reporter style."""
        pipeline = models.Pipeline.objects.first()

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}?style=reporter')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.PipelineOrderedSerializer(pipeline).data,
            response.json()
        )

    def test_get_test(self):
        """Test get all tests."""
        response = self.client.get('/api/1/test')
        self.assertEqual(
            response.json()['results'],
            {
                'tests': serializers.TestSerializer(models.Test.objects.all(), many=True).data
            }
        )

    def test_get_single_test(self):
        """Test get single tests."""
        response = self.client.get('/api/1/test/1')
        self.assertEqual(
            response.json(),
            serializers.TestSerializer(models.Test.objects.get(id=1)).data
        )

    def test_get_single_test_404(self):
        """Test get single tests. It doesn't exist."""
        response = self.client.get('/api/1/test/1234')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_get_issue_regex(self):
        """Test get issue regex."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        models.IssueRegex.objects.create(
            text_match='^([a-zA-Z_]+)+$',
            file_name_match='^([a-zA-Z_]+)+$',
            test_name_match='^([a-zA-Z_]+)+$',
            issue=issue,
        )

        response = self.client.get('/api/1/issue/regex')
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': {
                    'issue_regexes': [
                        {
                            'id': 1,
                            'text_match':  '^([a-zA-Z_]+)+$',
                            'file_name_match':  '^([a-zA-Z_]+)+$',
                            'test_name_match':  '^([a-zA-Z_]+)+$',
                            'issue': serializers.IssueSerializer(issue).data
                        }
                    ]
                }
            }
        )


class TestIssueAPI(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Tests for the Issue endpoints."""

    def test_list_issues(self):
        """Test list issues."""
        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0),
            gittree=models.GitTree.objects.create(name='Tree 1')
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        models.Issue.objects.create(description='other foo bar', ticket_url='http://other.url', kind=issue_kind)

        response = self.client.get('/api/1/issue')

        self.assertEqual(
            response.json()['results'],
            {'issues': serializers.IssueSerializer(models.Issue.objects.all(), many=True).data},
        )

    def test_get_issue(self):
        """Test get issue."""
        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0),
            gittree=models.GitTree.objects.create(name='Tree 1')
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        response = self.client.get(f'/api/1/issue/{issue.id}')

        self.assertEqual(
            response.json(),
            serializers.IssueSerializer(issue).data,
        )

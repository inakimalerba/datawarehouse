"""Test the views module."""
from datawarehouse import models
from tests import utils


class TestViewsInKCIDB(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test context data in kcidb views."""

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.create(name='redhat')

        self.revision = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df3'
        )

        self.revision_2 = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df4',
            valid=False,
        )

        self.revision_3 = models.KCIDBRevision.objects.create(
            origin=origin, id='decd6167bf4f6bec1284006d0522381b44660df5',
            valid=True,
        )

        self.build_1 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-1'
        )
        self.build_2 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-2',
            valid=False,
        )
        self.build_3 = models.KCIDBBuild.objects.create(
            origin=origin, revision=self.revision, id='redhat:build-3',
            valid=True,
        )
        self.test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-1'
        )
        self.test_2 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-2',
            status=models.TestResult.objects.create(name='FAIL')
        )
        self.test_3 = models.KCIDBTest.objects.create(
            origin=origin, build=self.build_1, id='redhat:test-3',
            status=models.TestResult.objects.create(name='PASS')
        )

        issue_kind = models.IssueKind.objects.create(description='fail', tag='fail')

        self.issues = [
            models.Issue.objects.create(
                description=f'foo bar {i}', ticket_url=f'http://some.url.{i}', kind=issue_kind
            ) for i in range(5)
        ]

    def _assign_issues(self, issues):
        """Given a dict of object_name->list(issue_id), assign the issue to the object."""
        for object_name, issue_ids in issues.items():
            getattr(self, object_name).issues.add(
                *[self.issues[id] for id in issue_ids]
            )

    def test_revision_view(self):
        """Test revisions view."""
        self._assign_issues({
            'revision': [0],
            'build_1': [1, 2],
            'build_2': [1],
            'test_1': [3],
            'test_2': [3, 4],
        })

        response = self.client.get(f'/kcidb/revisions/{self.revision.iid}')
        self.assertContextEqual(
            response.context,
            {
                'issue_occurrences': [
                    {'issue': self.issues[4], 'revisions': [], 'builds': [], 'tests': [self.test_2]},
                    {'issue': self.issues[3], 'revisions': [], 'builds': [], 'tests': [self.test_1, self.test_2]},
                    {'issue': self.issues[2], 'revisions': [], 'builds': [self.build_1], 'tests': []},
                    {'issue': self.issues[1], 'revisions': [], 'builds': [self.build_2, self.build_1], 'tests': []},
                    {'issue': self.issues[0], 'revisions': [self.revision], 'builds': [], 'tests': []},
                ],
                'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                'tests': [self.test_1, self.test_2, self.test_3],
                'tests_failed': [self.test_1, self.test_2],
                'builds': [self.build_3, self.build_2, self.build_1],
                'builds_failed': [self.build_2, self.build_1],
                'revision': self.revision,
                'revisions_failed': [self.revision],  # valid = None
            },
        )

        response = self.client.get(f'/kcidb/revisions/{self.revision_2.iid}')
        self.assertContextEqual(
            response.context,
            {
                'revisions_failed': [self.revision_2],  # valid = False
            },
        )

        response = self.client.get(f'/kcidb/revisions/{self.revision_3.iid}')
        self.assertContextEqual(
            response.context,
            {
                'revisions_failed': [],  # valid = True
            },
        )

    def test_revision_view_no_issues(self):
        """Test revisions view without issues."""
        response = self.client.get(f'/kcidb/revisions/{self.revision.iid}')
        self.assertEqual(response.context['issue_occurrences'], [])

    def test_build_view(self):
        """Test build view."""
        self._assign_issues({
            'build_1': [1, 2],
            'test_1': [3],
            'test_2': [3, 4],
        })

        response = self.client.get(f'/kcidb/builds/{self.build_1.iid}')
        self.assertContextEqual(
            response.context,
            {
                'issue_occurrences': [
                    {'issue': self.issues[4], 'builds': [], 'tests': [self.test_2]},
                    {'issue': self.issues[3], 'builds': [], 'tests': [self.test_1, self.test_2]},
                    {'issue': self.issues[2], 'builds': [self.build_1], 'tests': []},
                    {'issue': self.issues[1], 'builds': [self.build_1], 'tests': []},
                ],
                'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                'tests': [self.test_1, self.test_2, self.test_3],
                'tests_failed': [self.test_1, self.test_2],
                'build': self.build_1,
                'builds_failed': [self.build_1],  # valid = None
            },
        )

        response = self.client.get(f'/kcidb/builds/{self.build_2.iid}')
        self.assertContextEqual(
            response.context,
            {
                'builds_failed': [self.build_2],  # valid = False
            },
        )

        response = self.client.get(f'/kcidb/builds/{self.build_3.iid}')
        self.assertContextEqual(
            response.context,
            {
                'builds_failed': [],  # valid = True
            },
        )

    def test_build_view_no_issues(self):
        """Test build view without issues."""
        response = self.client.get(f'/kcidb/builds/{self.build_1.iid}')
        self.assertEqual(response.context['issue_occurrences'], [])

    def test_test_view(self):
        """Test test view."""
        self._assign_issues({
            'test_1': [3, 4],
        })

        response = self.client.get(f'/kcidb/tests/{self.test_1.iid}')
        self.assertContextEqual(
            response.context,
            {
                'issue_occurrences': [
                    {'issue': self.issues[4], 'tests': [self.test_1]},
                    {'issue': self.issues[3], 'tests': [self.test_1]},
                ],
                'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                'test': self.test_1,
                'tests_failed': [self.test_1],  # status = None
            },
        )

        response = self.client.get(f'/kcidb/tests/{self.test_2.iid}')
        self.assertContextEqual(
            response.context,
            {
                'tests_failed': [self.test_2],  # status = FAIL
            },
        )

        response = self.client.get(f'/kcidb/tests/{self.test_3.iid}')
        self.assertContextEqual(
            response.context,
            {
                'tests_failed': [],  # status = PASS
            },
        )

    def test_test_view_no_issues(self):
        """Test test view without issues."""
        response = self.client.get(f'/kcidb/tests/{self.test_1.iid}')
        self.assertEqual(response.context['issue_occurrences'], [])

    def test_create_issues_revision(self):
        """Test linking an issue to a revision."""
        self.assert_authenticated_post(
            302, 'change_kcidbrevision',
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[0].id,
                'revision_iids': [
                    self.revision.iid
                ],
            }
        )
        self.assertEqual(
            self.issues[0],
            self.revision.issues.get()
        )

    def test_create_issues_build(self):
        """Test linking an issue to some builds."""
        self.assert_authenticated_post(
            302, 'change_kcidbbuild',
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[0].id,
                'build_iids': [
                    self.build_1.iid,
                    self.build_2.iid,
                ]
            }
        )

        self.assertEqual(self.issues[0], self.build_1.issues.get())
        self.assertEqual(self.issues[0], self.build_2.issues.get())
        self.assertFalse(self.build_3.issues.exists())

    def test_create_issues_test(self):
        """Test linking an issue to some tests."""
        self.assert_authenticated_post(
            302, 'change_kcidbtest',
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[0].id,
                'test_iids': [
                    self.test_1.iid,
                    self.test_2.iid,
                ]
            }
        )

        self.assertEqual(self.issues[0], self.test_1.issues.get())
        self.assertEqual(self.issues[0], self.test_2.issues.get())
        self.assertFalse(self.test_3.issues.exists())

    def test_create_issues_all(self):
        """Test linking an issue to revisions, builds and tests."""
        self.assert_authenticated_post(
            302,
            [
                'change_kcidbrevision',
                'change_kcidbbuild',
                'change_kcidbtest',
            ],
            '/kcidb/issues/occurrences',
            {
                'issue_id': self.issues[1].id,
                'revision_iids': [
                    self.revision.iid,
                ],
                'build_iids': [
                    self.build_1.iid,
                    self.build_2.iid,
                ],
                'test_iids': [
                    self.test_1.iid,
                    self.test_2.iid,
                ],
            }
        )

        for elem in (self.revision, self.build_1, self.build_2, self.test_1, self.test_2):
            self.assertEqual(self.issues[1], elem.issues.get())

        for elem in (self.build_3, self.test_3):
            self.assertFalse(elem.issues.exists())

    def test_remove_issues_all(self):
        """Test unlinking an issue from revision, builds and tests."""
        self.revision.issues.add(self.issues[0])
        self.build_1.issues.add(self.issues[0])
        self.build_2.issues.add(self.issues[0])
        self.test_1.issues.add(self.issues[0])
        self.test_2.issues.add(self.issues[0])

        self.assert_authenticated_post(
            302,
            [
                'change_kcidbrevision',
                'change_kcidbbuild',
                'change_kcidbtest',
            ],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': self.issues[0].id,
                'revision_iids': [
                    self.revision.iid,
                ],
                'build_iids': [
                    self.build_1.iid,
                    self.build_2.iid,
                ],
                'test_iids': [
                    self.test_1.iid,
                    self.test_2.iid,
                ],
            }
        )

        for elem in (self.revision, self.build_1, self.build_2, self.test_1, self.test_2):
            self.assertFalse(elem.issues.exists())


class TestRevisionsList(utils.TestCase):
    """Test the revisions list view."""

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.create(name='redhat')

        models.KCIDBRevision.objects.bulk_create(
            [
                models.KCIDBRevision(origin=origin, id=f'decd6167bf4f6bec1284006d0522381b44660d{index}')
                for index in range(40)
            ]
        )
        self.revisions = models.KCIDBRevision.objects.all()

    def test_revision_list(self):
        """Test revisions list."""
        response = self.client.get('/kcidb/revisions')
        self.assertContextEqual(
            response.context,
            {
                'revisions': self.revisions[:30],
            },
        )

    def test_revision_list_page_2(self):
        """Test revisions list pagination."""
        response = self.client.get('/kcidb/revisions?page=2')
        self.assertContextEqual(
            response.context,
            {
                'revisions': self.revisions[30:],
            },
        )

    def test_revision_list_aggregated(self):
        """Test revisions list returns aggregated results."""
        response = self.client.get('/kcidb/revisions')
        self.assertTrue(
            hasattr(
                response.context['revisions'][0],
                'stats_tests_fail_count'
            )
        )


class TestRevisionsFailures(utils.TestCase):
    """Test the revisions failures list view."""

    def setUp(self):
        """Set up data."""
        cases = {
            # rev: invalid
            'decd6167bf4f6bec1284006d0522381b44660df1': {'valid': False},
            # rev: ok, build: invalid
            'decd6167bf4f6bec1284006d0522381b44660df2': {
                'builds': {
                    'redhat-1': {'valid': False}
                },
            },
            # rev: ok, build: ok, test: fail
            'decd6167bf4f6bec1284006d0522381b44660df3': {
                'builds': {
                    'redhat-2': {'tests': {'redhat-a': {'status': 'FAIL'}}},
                }
            },
            # rev: ok, build: ok, test: error
            'decd6167bf4f6bec1284006d0522381b44660df4': {
                'builds': {
                    'redhat-3': {'tests': {'redhat-b': {'status': 'ERROR'}}},
                }
            },
            # rev: ok, build: ok, test: pass
            'decd6167bf4f6bec1284006d0522381b44660df5': {
                'builds': {
                    'redhat-4': {'tests': {'redhat-c': {'status': 'PASS'}}},
                }
            },
        }

        self._create_kcidb_objects(cases)

    @staticmethod
    def _create_kcidb_objects(cases):
        """Populate kcidb objects from a dict."""
        origin = models.KCIDBOrigin.objects.get_or_create(name='redhat')[0]
        for rev_id, rev_data in cases.items():

            # Create revision
            revision = models.KCIDBRevision.objects.create(
                origin=origin,
                id=rev_id,
                valid=rev_data.get('valid', True)
            )

            builds = rev_data.get('builds', {})
            for build_id, build_data in builds.items():

                # Create builds
                build = models.KCIDBBuild.objects.create(
                    origin=origin,
                    revision=revision,
                    id=build_id,
                    valid=build_data.get('valid', True),
                )

                tests = build_data.get('tests', {})
                for test_id, test_data in tests.items():

                    # Crate tests
                    models.KCIDBTest.objects.create(
                        origin=origin,
                        build=build,
                        id=test_id,
                        status=models.TestResult.objects.get_or_create(
                            name=test_data.get('status', 'PASS')
                        )[0]
                    )

    def test_failures_all(self):
        """Test failures list. All."""
        response = self.client.get('/kcidb/failures/all')
        self.assertContextEqual(
            response.context,
            {
                'revisions': models.KCIDBRevision.objects.filter(
                    id__in=(
                        'decd6167bf4f6bec1284006d0522381b44660df1',
                        'decd6167bf4f6bec1284006d0522381b44660df2',
                        'decd6167bf4f6bec1284006d0522381b44660df3',
                        'decd6167bf4f6bec1284006d0522381b44660df4',
                    )
                ),
            },
        )

    def test_failures_revisions(self):
        """Test failures list. Revisions."""
        response = self.client.get('/kcidb/failures/revision')
        self.assertContextEqual(
            response.context,
            {
                'revisions': models.KCIDBRevision.objects.filter(
                    id__in=(
                        'decd6167bf4f6bec1284006d0522381b44660df1',
                    )
                ),
            },
        )

    def test_failures_builds(self):
        """Test failures list. Builds."""
        response = self.client.get('/kcidb/failures/build')
        self.assertContextEqual(
            response.context,
            {
                'revisions': models.KCIDBRevision.objects.filter(
                    id__in=(
                        'decd6167bf4f6bec1284006d0522381b44660df2',
                    )
                ),
            },
        )

    def test_failures_tests(self):
        """Test failures list. Tests."""
        response = self.client.get('/kcidb/failures/test')
        self.assertContextEqual(
            response.context,
            {
                'revisions': models.KCIDBRevision.objects.filter(
                    id__in=(
                        'decd6167bf4f6bec1284006d0522381b44660df3',
                        'decd6167bf4f6bec1284006d0522381b44660df4',
                    )
                ),
            },
        )

    def test_pagination(self):
        """Test list pagination."""
        # Remove previous objects
        models.KCIDBRevision.objects.all().delete()

        cases = {
            f'decd6167bf4f6bec1284006d0522381b44660a{index}': {'valid': False}
            for index in range(30)
        }
        self._create_kcidb_objects(cases)

        response = self.client.get('/kcidb/failures/revision?page=2')

        self.assertContextEqual(
            response.context,
            {
                'revisions': models.KCIDBRevision.objects.all()[30:],
            },
        )

    def test_list_aggregated(self):
        """Test revisions list returns aggregated results."""
        response = self.client.get('/kcidb/failures/all')
        self.assertTrue(
            hasattr(
                response.context['revisions'][0],
                'stats_tests_fail_count'
            )
        )

    def test_unknown(self):
        """Test revisions list returns aggregated results."""
        response = self.client.get('/kcidb/failures/foobar')
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Not sure what foobar is.', response.content)

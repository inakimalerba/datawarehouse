"""Test kcidb serializers."""
import json
import pathlib
from unittest import mock

import kcidb
import responses

from datawarehouse import models
from datawarehouse import patches
from datawarehouse.api.kcidb import serializers
from tests import utils


def mock_patchwork():
    """Mock patchwork requests."""
    patches.PATCHWORK_API.url = 'http://patchwork.server/api'

    mocks = [
        ('http://patchwork.server/api/patches/2322797', 'patchwork_api_patches_2322797.json'),
        ('http://patchwork.server/api/patches/2322798', 'patchwork_api_patches_2322798.json'),
        ('http://patchwork.server/api/series/58545', 'patchwork_api_series_58545.json')
    ]

    for url, file_name in mocks:
        file_content = pathlib.Path(utils.ASSETS_DIR, file_name).read_text()
        responses.add(responses.GET, url, json=json.loads(file_content))


class TestSerializers(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test KCIDB-schemed serializers."""

    schema = kcidb.io.schema.v3.VERSION

    @responses.activate
    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set up data."""
        rev_data = {
            'id': ('d1c47b385764aaa488bce182d944fa22bb1325d1+'
                   'a18c010ffcf8852321ceee0820c766974567e3eabf348afefccfa2fec2b64e2b'),
            'origin': 'redhat',
            'tree_name': 'arm',
            'git_repository_url': 'https://repository.com/repo/kernel-rhel',
            'git_repository_branch': 'rhel-1.2.3',
            'git_commit_hash': '403cbf29a4e277ad4872515ec3854b175960bbdf',
            'git_commit_name': 'commit name',
            'patch_mboxes': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                              'name': 'mbox'}],
            'message_id': '<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>',
            'description': 'this is the description',
            'publishing_time': '2020-06-01T06:47:41.108Z',
            'discovery_time': '2020-06-01T06:47:41.108Z',
            'valid': True,
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>'],
            'log_url': 'http://log.server/log.name',
            'misc': {'pipeline': {'variables': {'cki_pipeline_type': 'patchwork'}}},
        }
        build_data = {
            'revision_id': ('d1c47b385764aaa488bce182d944fa22bb1325d1+'
                            'a18c010ffcf8852321ceee0820c766974567e3eabf348afefccfa2fec2b64e2b'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ...',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
        }
        test_data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'description': 'hostname.redhat.com'
            },
            'path': 'boot',
            'description': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ]
        }

        mock_patchwork()
        self.revision = models.KCIDBRevision.create_from_json(rev_data)
        self.build = models.KCIDBBuild.create_from_json(build_data)
        self.test = models.KCIDBTest.create_from_json(test_data)

    def test_revision(self):
        """Test KCIDBRevision object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'revisions': [
                serializers.KCIDBRevisionSerializer(
                    self.revision
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_build(self):
        """Test KCIDBBuild object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'builds': [
                serializers.KCIDBBuildSerializer(
                    self.build
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_test(self):
        """Test KCIDBTest object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'tests': [
                serializers.KCIDBTestSerializer(
                    self.test
                ).data
            ]
        }

        self.schema.validate_exactly(data)

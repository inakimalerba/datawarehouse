"""Test the views module."""
import json
from unittest import mock

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from tests import utils


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestSubmit(utils.TestCase):
    """Test submit endpoint."""

    def test_schema_version(self):
        """Test schema version is validated."""
        data = {'version': {'major': 1, 'minor': 0}}
        self.assert_authenticated_post(
            400, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        data = {'version': {'major': 3, 'minor': 0}}
        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def test_full_submit(self):
        """Test submitting all the data."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
            ],
            'builds': [
                {'origin': 'redhat', 'revision_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1'},
                {'origin': 'redhat', 'revision_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2'},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1'},
                {'origin': 'redhat', 'build_id': 'redhat:build-2', 'id': 'redhat:test-2'},
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        for revision in data['revisions']:
            self.assertTrue(models.KCIDBRevision.objects.filter(id=revision['id']).exists())

        for build in data['builds']:
            self.assertTrue(models.KCIDBBuild.objects.filter(id=build['id']).exists())

        for test in data['tests']:
            self.assertTrue(models.KCIDBTest.objects.filter(id=test['id']).exists())

    def test_error_already_submitted(self):
        """Test submitting an already existing object."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
            ]
        }

        self.assertFalse(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        response = self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )
        self.assertTrue(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        self.assertDictEqual(
            {
                'warnings': [
                    [
                        'revisions',
                        'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                        'Entry with redhat https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3 already exists.'
                    ]
                ]
            },
            response.json()
        )

    def test_error_missing_parent(self):
        """Test submitting an object without it's parent."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'builds': [
                {'origin': 'redhat', 'revision_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:1234'},
            ]
        }

        self.assertFalse(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        response = self.assert_authenticated_post(
            400, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )
        self.assertDictEqual(
            {
                'errors': [
                    [
                        'builds',
                        'redhat:1234',
                        'Parent Revision is not present on the DB'
                    ]
                ]
            },
            response.json()
        )

    def test_retrigger(self):
        """Test submitting retriggered pipeline."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {
                    'origin': 'redhat',
                    'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'misc': {'pipeline': {'variables': {'retrigger': 'true'}}}
                },
            ]
        }
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {
                    'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'origin': 'redhat',
                    'valid': True,
                    'misc': {
                        'job': {
                            'id': 887316,
                            'name': 'merge',
                            'stage': 'merge',
                            'started_at': '2020-06-03T15:11:19.327Z',
                            'created_at': '2020-06-03T15:08:05.512Z',
                            'finished_at': '2020-06-03T15:14:55.148Z',
                            'duration': 215.820987,
                            'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                            'tag': '-209.el8',
                            'commit_message_title': '[redhat] kernel',
                            'kernel_version': None
                        },
                        'pipeline': {
                            'id': 592705,
                            'variables': {
                                'cki_pipeline_type': 'patchwork',
                                'retrigger': 'true'
                            },
                            'started_at': '2020-06-03T15:08:09.957Z',
                            'created_at': '2020-06-03T15:08:05.288Z',
                            'finished_at': None,
                            'duration': None,
                            'ref': 'retrigger-67609c5a-d0c6-43ed-b445-2c5c985871b4',
                            'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                            'project': {
                                'id': 2,
                                'path_with_namespace': 'cki-project/cki-pipeline',
                            }
                        }
                    }
                }
            ]
        }

        # Submit the pipeline with ALLOW_DEBUG_PIPELINES=False
        with mock.patch('datawarehouse.api.kcidb.views.settings.ALLOW_DEBUG_PIPELINES', False):
            response = self.assert_authenticated_post(
                201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )

        self.assertDictEqual(
            {
                'warnings': [
                    [
                        'revisions',
                        'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                        'Retrigger pipeline ignored'
                    ]
                ]
            },
            response.json()
        )

        self.assertFalse(
            models.KCIDBRevision.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )

        # Submit the pipeline with ALLOW_DEBUG_PIPELINES=True
        with mock.patch('datawarehouse.api.kcidb.views.settings.ALLOW_DEBUG_PIPELINES', True):
            self.assert_authenticated_post(
                201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )

        revision = models.KCIDBRevision.objects.get(
            id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
        )
        self.assertEqual(
            'retriggers',
            models.Pipeline.objects.get(merge_jobs__kcidb_revision=revision).gittree.name
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestEndpoints(utils.TestCase):
    """Test kcidb get/list endpoints."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set up data."""
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'decd6167bf4f6bec1284006d0522381b44660df3'},
            ],
            'builds': [
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1'},
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2'},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1'},
                {'origin': 'redhat', 'build_id': 'redhat:build-2', 'id': 'redhat:test-2'},
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def test_revisions_list(self):
        """Test revisions list endpoint."""
        revision = models.KCIDBRevision.objects.first()
        response = self.client.get('/api/1/kcidb/revisions')
        self.assertEqual(
            serializers.KCIDBRevisionSerializer([revision], many=True).data,
            response.json()['results']
        )

    def test_revisions_get(self):
        """Test revisions get endpoint. Both iid and id queries."""
        revision = models.KCIDBRevision.objects.first()
        response_iid = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}')
        response_id = self.client.get(f'/api/1/kcidb/revisions/{revision.id}')
        self.assertEqual(
            response_id.json(),
            response_iid.json()
        )
        self.assertEqual(
            serializers.KCIDBRevisionSerializer(revision).data,
            response_id.json()
        )

    def test_builds_list(self):
        """Test builds list endpoint."""
        revision = models.KCIDBRevision.objects.first()
        response = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}/builds')
        self.assertEqual(
            serializers.KCIDBBuildSerializer(revision.kcidbbuild_set.all(), many=True).data,
            response.json()['results']
        )

    def test_builds_get(self):
        """Test builds get endpoint. Both iid and id queries."""
        revision = models.KCIDBRevision.objects.first()
        build = revision.kcidbbuild_set.first()
        response_iid = self.client.get(f'/api/1/kcidb/builds/{build.iid}')
        response_id = self.client.get(f'/api/1/kcidb/builds/{build.id}')
        self.assertEqual(
            response_id.json(),
            response_iid.json()
        )
        self.assertEqual(
            serializers.KCIDBBuildSerializer(build).data,
            response_id.json()
        )

    def test_tests_list(self):
        """Test builds list endpoint."""
        revision = models.KCIDBRevision.objects.first()
        build = revision.kcidbbuild_set.first()
        response = self.client.get(f'/api/1/kcidb/builds/{build.id}/tests')
        self.assertEqual(
            serializers.KCIDBTestSerializer(build.kcidbtest_set.all(), many=True).data,
            response.json()['results']
        )

    def test_tests_get(self):
        """Test tests get endpoint. Both iid and id queries."""
        revision = models.KCIDBRevision.objects.first()
        build = revision.kcidbbuild_set.first()
        test = build.kcidbtest_set.first()
        response_iid = self.client.get(f'/api/1/kcidb/tests/{test.iid}')
        response_id = self.client.get(f'/api/1/kcidb/tests/{test.id}')
        self.assertEqual(
            response_id.json(),
            response_iid.json()
        )
        self.assertEqual(
            serializers.KCIDBTestSerializer(test).data,
            response_id.json()
        )
